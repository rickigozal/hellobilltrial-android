<!DOCTYPE html>

<html>

<head>
	<link href = "../public/bootstrap-4.1.1-dist/css/bootstrap.css">
	<title>Lihat PDF ku</title>

	<style type="text/css">
	table td, table th{
		border:1px solid black;
	}
</style>

</head>

<div class="row" style="padding:1px">
	<div class="left">
		<img src="../storage/app/logogram.png" width="75px"/>
	</div>
</div>
<body>


<h1>{{$header}}</h1>
			<br>

			@foreach ($branch as $key => $item)
						<h2>{{ $item->BranchName }}</h2>
						<br>

						HARDWARE
						<br>
						<table>
							<tr>
								<th>No</th>
								<th>HardwareName</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Discount Value</th>
								<th>SubTotal</th>
							</tr>
						@foreach ($item->hardware as $hw => $content)
						<tr>
							<th>{{$hw+1}}</th>
							<th>{{$content->HardwareName}}</th>
							<th>{{$content->Price}}</th>
							<th>{{$content->Quantity}}</th>
							<th>{{$content->DiscountTotal}}</th>
							<th>{{$content->SubTotal}}</th>
						</tr>
						@endforeach
						<tr>
							<th colspan="5" style="text-align:right;">Total</th>
							<th>{{$item->HardwareTotal}}</th>
						</tr>
						<br>
						LICENSE
						<br>
							<tr>
								<th>No</th>
								<th>License Name</th>
								<th>Price</th>
							</tr>
						@foreach ($item->license as $lc => $content)
						<tr>
							<th>{{$hw+1}}</th>
							<th>{{$content->LicenseName}}</th>
							<th>{{$content->Price}}</th>
						</tr>
						@endforeach
						<br>
						</table>

						<br>
						<br>
			@endforeach


    <br><br><br>

</body>

<div class="col-md-12 col-sm-12 col-xs-12">
	<table id="MainTable_category" class="table table-striped table-bordered" cellspacing="0">
		<thead>
			<tr>
				<td class="no-sort">Product Code</td>
				<td class="no-sort">Product Name</td>
				<td class="no-sort optionalDisplay" style="width:150px;">Action</td>
			</tr>
		</thead>
		<tbody id="Body">
			<tr id="row" class="looptemplate">
			  <td class="ProductCode"></td>
			  <td class="ProductName"></td>
			  <td class="Action optionalDisplay">
				<select class="form-control act" required>
				  <option value="0" selected disabled hidden>Select Action</option>
				  <!-- <option value="1">Edit</option>
				  <option value="2">Delete</option> -->
				</select>
			  </td>
			</tr>
		</tbody>
	</table>
</div>

</html>
