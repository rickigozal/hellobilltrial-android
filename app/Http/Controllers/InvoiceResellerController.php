<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Validator;

class InvoiceResellerController extends Controller
{
        public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }


    public function getInvoiceReseller(){
      $result = DB::table('InvoiceReseller')
      ->leftjoin('Quotation', 'Quotation.QuotationID', '=','InvoiceReseller.QuotationID')
      ->leftjoin('SalesVisitation','Quotation.SalesVisitationID','=','SalesVisitation.SalesVisitationID')
      ->leftjoin('User', 'SalesVisitation.UserID','=','User.UserID')
      ->leftjoin('UserType','User.UserTypeID','=','UserType.UserTypeID')
      ->leftjoin('Branch', 'SalesVisitation.BranchID','=','Branch.BranchID')
      ->select(['InvoiceResellerNO','InvoiceResellerID','Quotation.QuotationID','Quotation.QuotationNO','SalesVisitation.SalesVisitationID','Quotation.SalesVisitationID','UserFullName',
      'SalesVisitation.UserID','SalesVisitation.BranchID','BranchName','GrandTotal','GrandTotalReseller','Commission','InvoiceReseller.Status',
      'DeliveryDate','User.UserTypeID','UserTypeName'])
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'InvoiceReseller' => $result
      );
      return Response()->json($endresult);

    }

    public function getInvoiceResellerDetail(request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'InvoiceResellerID' => 'required'

      ];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $InvoiceResellerID = $input['InvoiceResellerID'];
      $result = DB::table('InvoiceReseller')
      ->leftjoin('Quotation', 'Quotation.QuotationID', '=','InvoiceReseller.QuotationID')
      ->leftjoin('SalesVisitation','Quotation.SalesVisitationID','=','SalesVisitation.SalesVisitationID')
      ->leftjoin('User', 'SalesVisitation.UserID','=','User.UserID')
      ->leftjoin('Branch', 'SalesVisitation.BranchID','=','Branch.BranchID')
      ->leftjoin('UserType', 'User.UserTypeID','=','UserType.UserTypeID')
      ->select(['InvoiceResellerNO','InvoiceResellerID','Quotation.QuotationID','Quotation.QuotationNO','SalesVisitation.SalesVisitationID','Quotation.SalesVisitationID','UserFullName',
      'SalesVisitation.UserID','SalesVisitation.BranchID','BranchName','GrandTotal','Commission','GrandTotalReseller','InvoiceReseller.Status',
      'DeliveryDate','User.UserTypeID','UserTypeName'])
      ->where('InvoiceResellerID',$InvoiceResellerID)
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'InvoiceResellerDetail' => $result
      );
      return Response()->json($endresult);

    }

    public function insertUpdateInvoiceReseller(request $request){
      $input = json_decode($request->getContent(), true);
      $rules = [
          'QuotationID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }

      $ID = @$input['InvoiceResellerID'];
      $Status = "Pending";
      $DueDate = now();
      $Code = "INVRES";
      $QuotationID = $input['QuotationID'];
      $Count = DB::table('InvoiceReseller')
      ->count();
      $Count = $Count+1;
      $InvoiceResellerNO = ($Code.$Count);
      if($ID == null){
        $result = DB::table('InvoiceReseller')
        ->insert(array('InvoiceResellerNO' => $InvoiceResellerNO,'QuotationID' => $QuotationID, 'Status' => $Status));
      }
      else{
        $result = DB::table('InvoiceReseller')
        ->where('InvoiceReseller',$ID)
        ->update(array('InvoiceResellerNO' => $InvoiceResellerNO, 'DueDate'=>$DueDate, 'Status' => $Status));
      }


          $result = $this->checkReturn($result);
          return Response()->json($result);

    }

    public function updateInvoiceResellerStatus(request $request){
      $input = json_decode($request->getContent(), true);
      $rules = [
          'InvoiceResellerID' => 'required',
          'Status' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }

      $InvoiceResellerID = $input['InvoiceResellerID'];
      $Status = $input['Status'];
      $DeliveryDate = now();
      $DueDate = Carbon::now()->addDays(30);


      if($Status == "Sent")
      {$result = DB::table('InvoiceReseller')
      ->where('InvoiceResellerID', $InvoiceResellerID)
      ->update(array('DeliveryDate' => $DeliveryDate, 'Status' => $Status));
      }
      Else
      {$result = DB::table('InvoiceReseller')
      ->where('InvoiceResellerID', $InvoiceResellerID)
      ->update(array('Status' => $Status));}

          $result = $this->checkReturn($result);
          return Response()->json($result);
    }

    //  public function DeleteQuotation(Request $request){
    //    $input = json_decode($this->request->getContent(),true);
    //    $QuotationID = @$input['QuotationID'];
    //    $result = DB::table('Quotation')->where('QuotationID', $QuotationID)->update(array(
    //               'Status' => 'D',
    //        ));
    //
    //   $result = $this->checkReturn($result);
    //
    //   return Response()->json($result);
    // }
}
