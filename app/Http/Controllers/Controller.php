<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use Mail;
use Validator;
use Illuminate\Http\Request;
use Service\Http\Requests;
use Storage;
use Config;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function upload_to_s3($input){
      $this->s3path="https://s3-us-west-2.amazonaws.com/hellobill-assets/";
    	$folder = @$input['Folder'];
    	$objectID = @$input['ObjectID'];
      $userID = @$input['UserID'];
      $time = time();
      $data = @$input['Data'];
    	$filename = @$input['Filename'];
      // $destinationPath = strtolower($folder)."/$userID/$objectID/$time.$filename";
      $destinationPath = strtolower($folder).'/'.$userID.'/'.$objectID.'/'.time().$filename;
      Storage::disk('s3')->put($destinationPath, base64_decode($data));
      // yang dibawah ini error kenapa toh?
      // dd($destinationPath);
      // $s3path = "https://s3-us-west-2.amazonaws.com/hellobill-assets/".$destinationPath;
      // $response = $this->generateResponse(0, [], "Success", ["url" => $s3path]);
      	$url = $this->s3path.$destinationPath;

        return $url;
        // return response()->json($response);

    }

    function getLastVal(){
        return DB::select('select lastval() AS id')[0]->id;
    }

    public function get_env($key){
        return $_ENV[$key];
    }




    public function unique($unique)
    {
        $unique['Table'];
        $result = DB::table($unique['Table'])
        ->where(DB::raw('lower("'.$unique['Column'].'")'),strtolower($unique['String']))
        ->where($unique['Table'].'ID','<>',$unique['ID'])
        ->where('Archived', null)
        ->count();

        if($result > 0)
        {$endresult = array(
          'Status' => 1,
          'Errors' => array(
            array('ID'=>$unique['Column'],'Message'=>$unique['Column'].'already exist')
          ),
          'Message' => "Duplicate");
            print_r(json_encode($endresult));
            die();
            }

        return($result);
    }

	public function checkToken($request){
        $input = array();
        $input = json_decode($request->getContent(),true);
        $rules = [
                    'Token' => 'required'
                ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            // validation error
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            print_r(json_encode($response));exit();
        }
        $response = $this->checkTokenDB(@$input['Token']);
        return $response;

    }

    public function checkForgetToken($request){
        $input = array();
        $input = json_decode($request->getContent(),true);
        $rules = [
                    'Token' => 'required'
                ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            // validation error
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            print_r(json_encode($response));exit();
        }
        $response = $this->checkForgetTokenDB(@$input['Token']);
        return $response;

    }

    public function coalesce($val, $ret){
        if(is_array($val)){
            if(count($val) > 0)
            return $val;
            else return $ret;
        }else {
            if($this->is_empty($val)||$val==null)
    			return $ret;
            else return trim($val);

        }
	}

    public function is_empty($val){
		return strlen(trim($val))==0 ;
	}


    function bill_cycle_clause($cycle){
		switch ($cycle) {
			case '1':
				return 'to_char("Invoice"."Date", \'YYYY-MM-DD\') as "Cycle", to_char("Invoice"."Date", \'DD-Mon-YYYY\') as "CycleName" ';

			case '2':
				return 'to_char("Invoice"."Date", \'YYYY-MM-W\') as "Cycle", to_char("Invoice"."Date", \'YYYY Mon "Week" W\') as "CycleName"' ;

			case '3':
				return 'to_char("Invoice"."Date", \'YYYY-MM\') as "Cycle", to_char("Invoice"."Date", \'Mon-YYYY\') as "CycleName"';

			case '4':
				return 'to_char("Invoice"."Date", \'YYYY\') as "Cycle", to_char("Invoice"."Date", \'YYYY\') as "CycleName"';

			default:
				# code...
				break;
		}
	}
	public function checkTokenDB($token){
		try{
            $response = collect(\DB::select('Select "AuthenticatorID", "UserID"
											from "Authenticator"
											where "AuthenticatorToken" = \''.$token.'\' AND "DisabledDate" is null'))->first();

			$errorList = array();
            $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
            $data = array(
                'LastAccessed' => $now
            );
            if(isset($response->AuthenticatorID)){
				DB::table('Authenticator')->where('AuthenticatorID', $response->AuthenticatorID)->update($data);
            return $response;
            } else {
                $errorList[] = array(
                    'ID' => 'Token',
                    'Message' => 'No Token Valid Found'
                );
                $error = $this->generateResponse(1, $errorList, "Please check input", null);
                print_r(json_encode($error));exit();
            }
		}catch(\Exception $e){
            return ['error'=>true, 'message'=>$e->getMessage()];
        }
		return $response;
	}


    public function checkForgetTokenDB($token){
		try{
            $response = collect(\DB::select('Select "ForgetAuthenticatorID", "UserID"
											from "ForgetAuthenticator"
											where "ForgetAuthenticatorToken" = \''.$token.'\' AND "ExpiredDate" > now()'))->first();

			$errorList = array();
            $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
            $data = array(
                'LastAccessed' => $now
            );
            if(isset($response->ForgetAuthenticatorID)){
                $result = $this->checkReturn($response);
                return Response()->json($result);
            } else {
                $errorList[] = array(
                    'ID' => 'Token',
                    'Message' => 'Token has expired'
                );
                $error = $this->generateResponse(1, $errorList, "Please check input", null);
                print_r(json_encode($error));exit();
            }
		}catch(\Exception $e){
            return ['error'=>true, 'message'=>$e->getMessage()];
        }
		return $response;
	}

    public function db_trans_start(){
        $res = \DB::select( \DB::raw('SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;'));
        $res = \DB::select( \DB::raw('BEGIN;'));
        return @$res;
	}

    public function db_trans_end(){
        $res = \DB::select( \DB::raw('END;'));
        return @$res;
	}

      function generateResponse($status, $errors, $message, $additional = null){
    	$result = ['Status'=>$status, 'Errors'=>$errors, 'Message'=>$message];
    	if(!empty($additional) && is_array($additional)) $result = array_merge($result, $additional);
    	return $result;
    }

        function checkErrors($rules,$errors){
		    $result = [];
		    foreach($rules as $key => $val){
			if(count($errors->get($key)) > 0){
				foreach($errors->get($key) as $err){
					$temp = ["ID" => $key, "Message" => $err];
					array_push($result, $temp);
				}
			}
		}
		return $result;
	}
  public function checkReturn($result){
  if($result == true){
      $result = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success"
      );
  } else {
      $result = array(
          'Status' => 1,
          'Errors' => array(),
          'Message' => "Fail"
      );
    }
    return $result;
  }
}
