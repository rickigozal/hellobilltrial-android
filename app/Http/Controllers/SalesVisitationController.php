<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class SalesVisitationController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getVisitationStatus(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'SalesVisitationID' => 'nullable'
        ];


        $SalesVisitationID = @$input['SalesVisitationID'];
        if($SalesVisitationID === null)
        {
            $result = DB::table('Status')
            ->where('Category','SalesVisitation')
            ->orderby('Number','asc')
            ->get();
        }
        else{
            $status = DB::table('SalesVisitationDetail')
            ->select(['StatusID'])
            ->where('SalesVisitationID',$SalesVisitationID)
            ->get();

            for($i = 0;$i < count($status); $i++){
                $statusID[$i] = $status[$i]->StatusID;
            }

            $result = DB::table('Status')
            ->where('Category','SalesVisitation')
            ->wherenotin('StatusID',$statusID)
            ->orderby('Number','asc')
            ->get();
        }


        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'SalesVisitationStatus' => $result
        );

        return Response()->json($endresult);

    }

    public function getSalesVisitation(){
      $result = DB::table('SalesVisitation')
      ->select(['SalesVisitationID','SalesVisitationNO','SalesVisitation.BranchID','BranchName','SalesVisitation.UserID','User.UserFullName','StartDate','LastStatus',
                'StatusName','BriefDescription'])
      ->leftjoin('Branch','Branch.BranchID','=','SalesVisitation.BranchID')
      ->leftjoin('User','User.UserID','=','SalesVisitation.UserID')
      ->leftjoin('Status','SalesVisitation.LastStatus','=','Status.StatusID')
      ->where('SalesVisitation.Status',null)
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'SalesVisitation' => $result
      );

       return Response()->json($endresult);
    }


        public function getSalesVisitationQuery(request $request){
            $input = json_decode($request->getContent(),true);
            $rules = [
                'String' => 'required'
            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $errors = $validator->errors();
                $errorList = $this->checkErrors($rules, $errors);
                $additional = null;
                $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
                return response()->json($response);
            }
            $string = $input['String'];
          $result = DB::table('SalesVisitation')
          ->select(['SalesVisitationID','SalesVisitationNO','SalesVisitation.BranchID','BranchName','SalesVisitation.UserID','User.UserFullName','StartDate','LastStatus','BriefDescription'])
          ->leftjoin('Branch','Branch.BranchID','=','SalesVisitation.BranchID')
          ->leftjoin('User','User.UserID','=','SalesVisitation.UserID')
          ->where('SalesVisitation.Status',null)
          ->get();

          $endresult = array(
              'Status' => 0,
              'Errors' => array(),
              'Message' => "Success",
              'SalesVisitation' => $result
          );

           return Response()->json($endresult);
        }

public function getSalesVisitationDetail(Request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'SalesVisitationID' => 'required'
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);
  }
  $SalesVisitationID = $input['SalesVisitationID'];
  $result = DB::table('SalesVisitation')
  ->leftjoin('User','User.UserID', '=','SalesVisitation.UserID')
  ->leftjoin('Branch','Branch.BranchID', '=','SalesVisitation.BranchID')
  ->leftjoin('Brand','Branch.BrandID','=','Brand.BrandID')
  ->leftjoin('Status','SalesVisitation.LastStatus','=','StatusID')
  ->select(['SalesVisitationID','SalesVisitationNO','SalesVisitation.BranchID','Branch.BranchName','SalesVisitation.UserID','User.UserFullName',
            'BriefDescription','GPS','LastStatus','StatusName','StartDate','Branch.BrandID','BrandName'])
  ->orderBy('SalesVisitationID', 'desc')
  ->where('SalesVisitationID',$SalesVisitationID)
  ->get();
  $endresult = array(
      'Status' => 0,
      'Errors' => array(),
      'Message' => "Success",
      'SalesVisitationDetail' => $result
  );

   return Response()->json($endresult);
return Response()->json($result);
}

public function getSalesVisitationDetailByID(Request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'SalesVisitationDetailID' => 'required'
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);
  }
  $SalesVisitationDetailID = $input['SalesVisitationDetailID'];
  $result = DB::table('SalesVisitationDetail')
  ->leftjoin('SalesVisitation','SalesVisitation.SalesVisitationID','=','SalesVisitationDetail.SalesVisitationID')
  ->leftjoin('User','User.UserID', '=','SalesVisitation.UserID')
  ->leftjoin('Branch','Branch.BranchID', '=','SalesVisitation.BranchID')
  ->select(['SalesVisitationDetailID','SalesVisitationDetail.SalesVisitationID','SalesVisitationNO','SalesVisitation.BranchID','Branch.BranchName','SalesVisitation.UserID','User.UserFullName',
            'BriefDescription','GPS','LastStatus','StartDate','SalesVisitationDetail.Status','SalesVisitationDetail.Description','Date'])
  ->where('SalesVisitationDetail.SalesVisitationDetailID',$SalesVisitationDetailID)
  ->get();

  $endresult = array(
      'Status' => 0,
      'Errors' => array(),
      'Message' => "Success",
      'SalesVisitationDetailbyID' => $result
  );

   return Response()->json($endresult);
}

public function getSalesVisitationDetailBySalesVisitationID(Request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'SalesVisitationID' => 'required'
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);
  }
  $SalesVisitationID = $input['SalesVisitationID'];
  $result = DB::table('SalesVisitationDetail')
  ->leftjoin('SalesVisitation','SalesVisitation.SalesVisitationID','=','SalesVisitationDetail.SalesVisitationID')
  ->leftjoin('User','User.UserID', '=','SalesVisitation.UserID')
  ->leftjoin('Branch','Branch.BranchID', '=','SalesVisitation.BranchID')
  ->leftjoin('Status','Status.StatusID','=','SalesVisitationDetail.StatusID')
  ->select(['SalesVisitationDetailID','SalesVisitationDetail.SalesVisitationID','SalesVisitationNO','SalesVisitation.BranchID','Branch.BranchName','SalesVisitation.UserID','User.UserFullName',
            'BriefDescription','GPS','LastStatus','StartDate','SalesVisitationDetail.StatusID','StatusName','SalesVisitationDetail.Description','Date'])
  ->where('SalesVisitation.SalesVisitationID',$SalesVisitationID)
  ->get();

  $endresult = array(
      'Status' => 0,
      'Errors' => array(),
      'Message' => "Success",
      'SalesVisitationDetailbySalesID' => $result
  );

   return Response()->json($endresult);
}

public function insertUpdateSalesVisitationDetail(request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'SalesVisitationID' => 'required',
      'Date' => 'date|date_format:Y-m-d|nullable',
      'Description' => 'required'
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);
  }

    $SalesVisitationID = $input['SalesVisitationID'];
    $Date = $input['Date'];
    $StatusID = @$input['StatusID'];
    $Description = @$input['Description'];

  $id = @$input['SalesVisitationDetailID'];

  if($id == null)
  {$result = DB::table('SalesVisitationDetail')
  ->insert(array('SalesVisitationID' => $SalesVisitationID,
                 'Date' => $Date,
                 'StatusID' => $StatusID,
                 'Description' => $Description));
               $result = DB::table('SalesVisitation')
             ->where('SalesVisitationID',$SalesVisitationID)
           ->update (array('LastStatus' => $StatusID,'BriefDescription' => @$input['Description']));
         }
  else{$result = DB::table('SalesVisitationDetail')
  ->where('SalesVisitationDetailID',$id)
  ->update(array('SalesVisitationID' => $SalesVisitationID,
                  'Description' => $Description,
                  'Date' => $Date));
    $idmax = DB::table('SalesVisitationDetail')
    ->where('SalesVisitationID',$SalesVisitationID)
    ->max('SalesVisitationDetailID');
    if ($id = $idmax){
        $result = DB::table('SalesVisitation')
       ->where('SalesVisitationID',$SalesVisitationID)
       ->update(array('BriefDescription' => @$input['Description']));
    }

}
  $result = $this->checkReturn($result);
return response()->json($result);
}


    public function InsertUpdateSalesVisitation(Request $request){
       $input = json_decode($request->getContent(), true);
       $rules = [
         'BranchID' => 'required',
         'Date' => 'date|date_format:Y-m-d|nullable',
         'StatusID' => 'required'
          ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $count = DB::table('SalesVisitation')
       ->max('SalesVisitationID');
       $count = $count+1;
       $code = "SLV".$count;
       $paramheader = array (
         'SalesVisitationNO' => $code,
         'UserID' => $this->param->UserID,
         'BranchID' => $input['BranchID'],
         'StartDate' => now(),
         'LastStatus' => @$input['StatusID'],
         'BriefDescription' => @$input['Description']
       );

       $ID = @$input['SalesVisitationID'];
       if($ID == null){
           $result = DB::table('SalesVisitation')->insert($paramheader);
           $ID = $this->getlastval();
           $paramdetail = array(
               'SalesVisitationID' => $ID,
               'Date' => $input['Date'],
               'StatusID' => $paramheader['LastStatus'],
               'Description' => @$input['Description']
           );
           $result = DB::table('SalesVisitationDetail')
           ->insert($paramdetail);
       }
       else {$result = DB::table('SalesVisitation')->where('SalesVisitationID', $ID)->update($paramheader);}
       $result = $this->checkReturn($result);

       return Response()->json($result);
    }

    public function DeleteSalesVisitation(Request $request){
         $input = json_decode($this->request->getContent(),true);
         $rules = [
           'SalesVisitationID' => 'required'
         ];
         $validator = Validator::make($input, $rules);
         if ($validator->fails()) {
             $errors = $validator->errors();
             $errorList = $this->checkErrors($rules, $errors);
             $additional = null;
             $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
             return response()->json($response);
         }
         $SalesVisitationID = $input['SalesVisitationID'];
         $param = array('Status' => 'D');
         $result = DB::table('SalesVisitation')->where('SalesVisitationID', $SalesVisitationID)->update($param);

        $result = $this->checkReturn($result);

        return Response()->json($result);

    }
}
