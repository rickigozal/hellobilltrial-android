<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class ServiceController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getService(Request $request){
        $input = json_decode($request->getContent(),true);

        $isGetAll = @$input['isGetAllService'];
        if($isGetAll == true)
        {
            $result = DB::table('Service')->select(['ServiceID','ServiceName','Price','ResellerPrice','Duration','Status'])
            ->where('Archived',null)
            ->orderby('ServiceID','asc')
            ->get();
        }
        else{
            $result = DB::table('Service')->select(['ServiceID','ServiceName','Price','ResellerPrice','Duration','Status'])
            ->where('Archived',null)
            ->where('Status',null)
            ->orderby('ServiceID','asc')
            ->get();
        }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Service' => $result
      );
    return Response()->json($endresult);
    }

    public function getServiceDetail(request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'ServiceID' => 'required',
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $ServiceID = $input['ServiceID'];
      $result = DB::table('Service')
      ->select(['ServiceID','ServiceName','Price','ResellerPrice','Duration'])
      ->where('ServiceID',$ServiceID)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Service' => $result
      );
    return Response()->json($endresult);

    }

    public function InsertUpdateService(request $request){

        $input = json_decode($request->getContent(), true);
        $rules = [
        'ServiceName' => 'required',
        'Price' => 'required',
        'ResellerPrice' => 'required',
        'Duration' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $ID = @$input['ServiceID'];
        $unique = array(
            'Table' => "Service",
            'ID' => $ID,
            'Column' => "ServiceName",
            'String' => $input['ServiceName']
        );
        $uniqueHardwareName = $this->unique($unique);
        $param = array(
            'ServiceName' => $input['ServiceName'],
            'Price' => $input['Price'],
            'ResellerPrice' => $input['ResellerPrice'],
            'Duration' => $input['Duration']
        );

        if($ID == null){$result = DB::table('Service')
        ->insert($param);}
        else {$result = DB::table('Service')
          ->where('ServiceID', $ID)
          ->update($param);
        }

        $result = $this->checkReturn($result);

        return Response()->json($result);

      }
      public function ActivateDeactivateService(Request $request){
           $input = json_decode($this->request->getContent(),true);
           $rules = ['ServiceID' => 'required'];
           $validator = Validator::make($input, $rules);
           if ($validator->fails()) {
               $errors = $validator->errors();
               $errorList = $this->checkErrors($rules, $errors);
               $additional = null;
               $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
               return response()->json($response);
           }
           $ServiceID = @$input['ServiceID'];

           $ServiceStatus = DB::table('Service')
           ->where('ServiceID',$ServiceID)
           ->first()->Status;

           if($ServiceStatus === null)
           {
               $param = array('Status' => 'D');
               $result = DB::table('Service')->where('ServiceID', $ServiceID)->update($param);
           }
           else{
               $param = array('Status' => null);
               $result = DB::table('Service')->where('ServiceID', $ServiceID)->update($param);
           }


          $result = $this->checkReturn($result);

          return Response()->json($result);

      }
      public function DeleteService(Request $request){
           $input = json_decode($this->request->getContent(),true);
           $rules = ['ServiceID' => 'required'];
           $validator = Validator::make($input, $rules);
           if ($validator->fails()) {
               $errors = $validator->errors();
               $errorList = $this->checkErrors($rules, $errors);
               $additional = null;
               $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
               return response()->json($response);
           }
           $ServiceID = @$input['ServiceID'];
           $param = array('Archived' => now());

           $result = DB::table('Service')->where('ServiceID', $ServiceID)->update($param);

          $result = $this->checkReturn($result);

          return Response()->json($result);

      }
}
