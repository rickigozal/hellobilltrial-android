<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class ForgetPasswordController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkForgetToken($request);
        $this->request = $request;
    }

    public function setPassword(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'Password' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$/',
            'Username' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $Password = password_hash($input['Password'], PASSWORD_BCRYPT);
        $result = DB::table('User')
        ->where(DB::raw('lower("Username")'),strtolower($input['Username']))
        ->update(array('Password' => $Password));

        $result = $this->checkReturn($result);
        return response()->json($result);

    }


}
