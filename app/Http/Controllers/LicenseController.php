<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class LicenseController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getLicense(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'ProductID' => 'nullable'
        ];
        if(@$input['ProductID'] === null)
        {
            $result = DB::table('License')
            ->leftjoin('Product','License.ProductID','=','Product.ProductID')
            ->select(['LicenseID','LicenseCode','LicenseName','License.ProductID','ProductCode','ProductName','Price','Duration','ResellerPrice',
                      'Free'])
            ->where('License.Status',null)
            ->orderby('LicenseID','desc')
            ->get();
        }
        else{
            $result = DB::table('License')
            ->leftjoin('Product','License.ProductID','=','Product.ProductID')
            ->select(['LicenseID','LicenseCode','LicenseName','License.ProductID','ProductCode','ProductName','Price','Duration','ResellerPrice',
                      'Free'])
            ->where('License.Status',null)
            ->where('License.ProductID', @$input['ProductID'])
            ->orderby('LicenseID','desc')
            ->get();
        }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'License' => $result
      );
    return Response()->json($endresult);
    }

    public function getLicenseDetail(Request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'LicenseID' => 'required',
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $LicenseID = $input['LicenseID'];
      $result = DB::table('License')
      ->leftjoin('Product','License.ProductID','=','Product.ProductID')
      ->select(['LicenseID','LicenseCode','LicenseName','License.ProductID','ProductCode','ProductName','Price','Duration','ResellerPrice','Free'])
      ->where('LicenseID',$LicenseID)
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'License' => $result
      );
      return Response()->json($endresult);

    }

    public function InsertUpdateLicense(Request $request){
      $input = json_decode($request->getContent(), true);
      $rules = [
      'LicenseName' => 'required',
      'ResellerPrice' => 'numeric|required',
      'Price' => 'required|numeric',
      'Duration' => 'required|numeric',
      'ProductID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $id = @$input['LicenseID'];
      $unique = array(
          'Table' => "License",
          'ID' => $id,
          'Column' => "LicenseName",
          'String' => $input['LicenseName']
      );
      $uniqueLicenseName = $this->unique($unique);
      $unique['Column'] = "LicenseCode";
      $unique['String'] = $input['LicenseCode'];
      $uniqueLicenseCode = $this->unique($unique);

      $LicenseCode = $input['LicenseCode'];
      $LicenseName = $input['LicenseName'];
      $Price = $input['Price'];
      $ResellerPrice = $input['ResellerPrice'];
      $Duration = $input['Duration'];
      $ProductID = $input['ProductID'];


      if ($id == null){$result = DB::table('License')->insert(array(
      'LicenseCode' => $LicenseCode,
      'LicenseName' => $LicenseName,
      'Price' => $Price,
      'ResellerPrice' => $ResellerPrice,
      'Duration' => $Duration,
      'ProductID' => $ProductID,
      'Free' => @$input['Free']
      ));}
      else {$result = DB::table('License')->where('LicenseID', $id)->update(array(
      'LicenseCode' => $LicenseCode,
      'LicenseName' => $LicenseName,
      'Price' => $Price,
      'ResellerPrice' => $ResellerPrice,
      'Duration' => $Duration,
      'ProductID' => $ProductID,
      'Free' => @$input['Free']
      ));}

    $result = $this->checkReturn($result);
    return Response()->json($result);

    }
    public function DeleteLicense(Request $request){
         $input = json_decode($this->request->getContent(),true);
         $LicenseID = @$input['LicenseID'];
         $result = DB::table('License')->where('LicenseID', $LicenseID)->update(array(
                    'Status' => 'D', 'Archived' => now()
             ));

             $result = $this->checkReturn($result);

             return Response()->json($result);


    }
}
