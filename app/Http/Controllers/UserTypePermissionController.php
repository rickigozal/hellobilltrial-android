<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class UserTypePermissionController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getPermission(){
      $ID = $this->param->UserID;

      // $permission = DB::table('User')
      // ->leftjoin('UserType','UserType.UserTypeID','=','User.UserTypeID')
      // ->leftjoin('UserTypePermission','UserType.UserTypeID','=','UserTypePermission.UserTypeID')
      // ->leftjoin('Permission','UserTypePermission.PermissionID','=','Permission.PermissionID')
      // ->select(['UserTypePermission.PermissionID','PermissionName','URL','Entry'])
      // ->get();

      $permission= DB::table('Permission')
      ->leftjoin('Parent','Permission.ParentID','=','Parent.ParentID')
      ->select()
      ->where('Entry','<>',null)
      ->orderby('Permission.Number','asc')
      ->orderby('NumberParent','asc')
      ->get();

      $entry = DB::table('Permission')
      ->leftjoin('Parent','Permission.ParentID','=','Parent.ParentID')
      ->distinct('Entry')
      ->select(['Entry','NumberParent'])
      ->where('Entry','<>',null)
      ->orderby('NumberParent', 'asc')
      ->get();


      foreach($entry as $k=>$v){
      	$v->Permissions = [];
      	$entry[$v->Entry] = $v;
      	unset($entry[$k]);
      }

      foreach($permission as $v){
      	$entry[$v->Entry]->Permissions[]=$v;
      }

      $entry = array_values(json_decode(json_encode($entry), true));

      $userpermission = DB::table('User')
      ->leftjoin('UserType','UserType.UserTypeID','=','User.UserTypeID')
      ->leftjoin('UserTypePermission','UserType.UserTypeID','=','UserTypePermission.UserTypeID')
      ->leftjoin('Permission','UserTypePermission.PermissionID','=','Permission.PermissionID')
      ->select(['UserTypePermission.PermissionID','PermissionName','URL'])
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Permission' => $entry
      );
       return Response()->json($endresult);
    }

    public function getMenuPermission(){
        $ID = $this->param->UserID;
        $permission= DB::table('UserTypePermission')
        ->leftjoin('UserType','UserType.UserTypeID','=','UserTypePermission.UserTypeID')
        ->leftjoin('User','User.UserTypeID','=','UserType.UserTypeID')
        ->leftjoin('Permission','Permission.PermissionID','=','UserTypePermission.PermissionID')
        ->select(['Permission.PermissionID','PermissionName','PermissionType','Entry','ParentID','Permission.URL','Permission.Number'])
        ->whereNotNull('Entry')
        ->where('User.UserID', $ID)
        ->where('PermissionType','Menu')
        ->orderby('Permission.Number', 'desc')
        ->get();
        foreach($permission->unique('URL') as $permission){
                $permission2[] = $permission;
        }
        // return $permission2;
        $parent = DB::table('Parent')
        ->leftjoin('Permission','Permission.ParentID','=','Parent.ParentID')
        ->leftjoin('UserTypePermission','UserTypePermission.PermissionID','=','Permission.PermissionID')
        ->leftjoin('UserType','UserTypePermission.UserTypeID','=','UserType.UserTypeID')
        ->leftjoin('User','UserType.UserTypeID','=','User.UserTypeID')
        ->distinct('Parent.ParentID')
        ->select(['Parent.ParentID','ParentName','Parent.URL','Parent.Icon'])
        ->where('UserID',$ID)
        ->orderby('ParentName','asc')
        ->get();


        foreach($parent as $k=>$v){
          $v->Permissions = [];
          $parent[$v->ParentID] = $v;
          unset($parent[$k]);
        }

        foreach($permission2 as $v){
          $parent[$v->ParentID]->Permissions[]=$v;
        }

        $parent = array_values(json_decode(json_encode($parent), true));

        $userpermission = DB::table('User')
        ->leftjoin('UserType','UserType.UserTypeID','=','User.UserTypeID')
        ->leftjoin('UserTypePermission','UserType.UserTypeID','=','UserTypePermission.UserTypeID')
        ->leftjoin('Permission','UserTypePermission.PermissionID','=','Permission.PermissionID')
        ->select(['UserTypePermission.PermissionID','PermissionName','URL'])
        ->get();

        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'Permission' => $parent
        );
         return Response()->json($endresult);
    }

        public function getPermissionbyUserTypeID(request $request){
          $input = json_decode($request->getContent(),true);
          $rules = [
              'UserTypeID' => 'required',
          ];

          $validator = Validator::make($input, $rules);
          if ($validator->fails()) {
              $errors = $validator->errors();
              $errorList = $this->checkErrors($rules, $errors);
              $additional = null;
              $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
              return response()->json($response);
          }
          $UserTypeID = $input['UserTypeID'];
          // $permission = DB::table('User')
          // ->leftjoin('UserType','UserType.UserTypeID','=','User.UserTypeID')
          // ->leftjoin('UserTypePermission','UserType.UserTypeID','=','UserTypePermission.UserTypeID')
          // ->leftjoin('Permission','UserTypePermission.PermissionID','=','Permission.PermissionID')
          // ->select(['UserTypePermission.PermissionID','PermissionName','URL','Entry'])
          // ->get();
          //
          // $permission= DB::table('Permission')
          // ->select()
          // ->where('Entry','<>',null)
          // ->get();

          $permission= DB::table('UserType')
          ->leftjoin('UserTypePermission','UserTypePermission.UserTypeID','=','UserType.UserTypeID')
          ->leftjoin('Permission','UserTypePermission.PermissionID','=','Permission.PermissionID')
          ->select(['UserTypePermission.PermissionID'])
          ->where('Entry','<>',null)
          ->where('UserType.UserTypeID',$UserTypeID)
          ->get();
          $permission = array_values(json_decode(json_encode($permission), true));
          $endresult = array(
              'Status' => 0,
              'Errors' => array(),
              'Message' => "Success",
              'Permission' => $permission
          );
           return Response()->json($endresult);
        }

  public function DeleteBrand(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $rules = [
         'BrandID' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $BrandID = @$input['BrandID'];
       $param = array('Status' => 'D','Archived' => now());
       $result = DB::table('Brand')->where('BrandID', $BrandID)->update($param);

      $result = $this->checkReturn($result);

      return Response()->json($result);

  }


}
