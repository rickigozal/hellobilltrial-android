<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class UserTypeController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getUserType(){
      $result = DB::table('UserType')->select(['UserTypeID','UserTypeCode','UserTypeName','Description'])
      ->where('Status',null)
      ->get();

       return Response()->json($result);
    }

    public function getUserTypeDetail(Request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'UserTypeID' => 'required',
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $UserTypeID = $input['UserTypeID'];
      $result = DB::table('UserType')
      ->select(['UserTypeID','UserTypeCode','UserTypeName','Description'])
      ->where('UserTypeID',$UserTypeID)
      ->get();

    return Response()->json($result);

    }

    public function InsertUpdateUserType(request $request){
      $input = json_decode($request->getContent(), true);
      $rules = [
      'UserTypeCode' => 'required',
      'UserTypeName' => 'required',
      'PermissionID' => 'array|distinct'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $id = @$input['UserTypeID'];
      $unique = array(
          'Table' => "UserType",
          'ID' => $id,
          'Column' => "UserTypeCode",
          'String' => $input['UserTypeCode']
      );
      $uniqueUserTypeCode = $this->unique($unique);

      $param = array(
        'UserTypeCode' => $input['UserTypeCode'],
        'UserTypeName' => $input['UserTypeName'],
        'Description' => @$input['Description']
      );
      if($id == null){
      $result = DB::table('UserType')->insert($param);
      $id = $this->getLastVal();
      }
      else{
        $result = DB::table('UserType')->where('UserTypeID',$id)->update($param);
        $result = DB::table('UserTypePermission')->where('UserTypeID',$id)->delete();
      }

          $temp = 'PermissionID';
      for($i = 0; $i < count($input[$temp]);$i++){
          $items = $input[$temp][$i];

          $insertPermission = array(
            'UserTypeID' => $id,
            $temp => @$items
          );
            $resultPermission = DB::table('UserTypePermission')->insert($insertPermission);
          }

      $result = $this->checkReturn($resultPermission);
      return Response()->json($result);
    }

    public function DeleteUserType(Request $request){
         $input = json_decode($this->request->getContent(),true);
         $UserTypeID = @$input['UserTypeID'];
         $result = DB::table('UserType')->where('UserTypeID', $UserTypeID)->update(array(
                    'Status' => 'D', 'Archived' => now()
             ));

        $result = $this->checkReturn($result);

        return Response()->json($result);

    }

}
