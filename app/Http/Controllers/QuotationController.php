<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class QuotationController extends Controller
{
        public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function generateQuotation(Request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'QuotationID' => 'required'
      ];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }

      $UserID = $this->param->UserID;

      $result = $this->generatePDFQuotation($input['QuotationID']);
      $result = json_decode($result);
      $result = $result->data;
      // return $result;

          $base64mentah = str_replace("\r\n","",$result);


              $pattern1 = "filename=\"";
              $pos = strpos($base64mentah,$pattern1);
              // return $pos;
              $pos2 = strpos($base64mentah,"pdf\"", $pos);
              $pos2=$pos2+4;
              $base64fix = substr($base64mentah,$pos2);

              $pos3 = strpos($base64mentah,"\"",$pos);
              $length = $pos2-$pos3;

              $filename = substr($base64mentah,$pos3,$length);
              $filename = str_replace("\"","",$filename);

              $arr = array(
                  'UserID' => $this->param->UserID,
                  'ObjectID' => $input['QuotationID'],
                  'Folder' => 'Quotation',
                  'Filename' => $filename,
                  'Data' =>  $base64fix
              );
              // return(@$input['Images']['Data']);
              // die();

              $path = $this->upload_to_s3($arr);
              $data = ['URL' => $path];

              $result = DB::table('Quotation')->where('QuotationID',$input['QuotationID'])->update($data);
              $link = $path;
              $response = $this->generateResponse(0, [], "Success", ['Quotation'=>$result]);



      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Link' => $path
      );

       return Response()->json($endresult);

    }

    public function generatePDFQuotation($ID){
        $Token = DB::table('Authenticator')
        ->where('AuthenticatorID',$this->param->AuthenticatorID)
        ->first()->AuthenticatorToken;
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/json',
                'content' => '{}'
            ));
        $context = stream_context_create($opts);
        $test = 'http://192.168.122.109/hellobill-tracking-sales/services/index.php/quotation/print_quotation/'.$ID.'/mime'.'/'.$Token;

        $data = file_get_contents($test,false,$context);
        return $data;
    }

    public function getQuotation(Request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'isGetAll' => 'required'
      ];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }


      $UserID = $this->param->UserID;
      $isGetAll = $input['isGetAll'];

      if($isGetAll === true){
          $result = DB::table('Quotation')
          ->leftjoin('Brand','Brand.BrandID' ,'=','Quotation.BrandID')
          ->leftjoin('Status','Quotation.StatusID','=','Status.StatusID')
          ->select(['Quotation.QuotationID','QuotationNO', 'Quotation.BrandID','BrandName','Brand.Email','GrandTotal','GrandTotalReseller','Quotation.StatusID',
                    'StatusName','Brand.Contact','Brand.Phone','QuotationDate','QuotationDueDate','Brand.Address','GrandTotalDiscount'])
          ->orderby('QuotationID','desc')
          ->where('Quotation.Archived',null)
          ->get();
          for($i=0;$i<count($result);$i++)
          {
              $QuotationID = $result[$i]->QuotationID;
              $user = DB::table('QuotationDetailUser AS QDU')
              ->leftjoin('User AS U','QDU.UserID','=','U.UserID')
              ->select(['QDU.UserID','UserFullName','Username','Email','Phone'])
              ->where('QuotationID',$QuotationID)
              ->orderby('QuotationDetailUserID','asc')
              ->get();
              $result[$i]->User = $user;
          }
      }
      else{
          $result = DB::table('Quotation')
          ->leftjoin('Brand','Brand.BrandID' ,'=','Quotation.BrandID')
          ->leftjoin('Status','Quotation.StatusID','=','Status.StatusID')
          ->leftjoin('QuotationDetailUser','QuotationDetailUser.QuotationID','=','Quotation.QuotationID')
          ->distinct('QuotationID')
          ->select(['Quotation.QuotationID','QuotationNO', 'Quotation.BrandID','BrandName','Brand.Email','Brand.Contact','Brand.Phone','GrandTotal','GrandTotalReseller','Quotation.StatusID',
                    'StatusName','QuotationDate','QuotationDueDate','Brand.Address','GrandTotalDiscount'])
          ->where('QuotationDetailUser.UserID',$UserID)
          ->orderby('QuotationID','desc')
          ->where('Quotation.Archived',null)
          ->get();
          for($i=0;$i<count($result);$i++)
          {
              $QuotationID = $result[$i]->QuotationID;
              $user = DB::table('QuotationDetailUser AS QDU')
              ->leftjoin('User AS U','QDU.UserID','=','U.UserID')
              ->select(['QDU.UserID','UserFullName','Username','Email','Phone'])
              ->where('QuotationID',$QuotationID)
              ->orderby('QuotationDetailUserID','asc')
              ->get();
              $result[$i]->User = $user;
          }

      }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Quotation' => $result
      );

       return Response()->json($endresult);

    }

    public function getQuotationDetailExcel(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'isGetAll' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $UserID = $this->param->UserID;
        $isGetAll = $input['isGetAll'];
        if($isGetAll === true){
            $header = DB::table('Quotation')
            ->leftjoin('Brand', 'Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('QuotationDetailUser', 'QuotationDetailUser.QuotationID','=' ,'Quotation.QuotationID')
            ->leftjoin('User','QuotationDetailUser.UserID','=','User.UserID')
            ->leftjoin('Status','Quotation.StatusID','=','Status.StatusID')
            ->select(['Quotation.QuotationID','QuotationNO', 'Quotation.BrandID','BrandName','Brand.Contact','Brand.Email','GrandTotal','GrandTotalReseller','Brand.Address', 'UserFullName','Quotation.StatusID',
                      'StatusName','UserFullName','QuotationDetailUser.UserID','Quotation.QuotationDueDate','Quotation.QuotationDate','Brand.Phone',
                      'GrandTotalDiscount','ProductID'])
            ->where('Quotation.Archived',null)
            ->get();
            // return $header;
            $headercount = count($header);
            for($i=0;$i<$headercount;$i++)
            {
                $QuotationID = $header[$i]->QuotationID;
                $item = $header[$i];
                $branch = DB::table('Quotation')
                ->leftjoin('Brand', 'Quotation.BrandID','=','Brand.BrandID')
                ->leftjoin('QuotationDetailUser', 'QuotationDetailUser.QuotationID','=' ,'Quotation.QuotationID')
                ->leftjoin('User','QuotationDetailUser.UserID','=','User.UserID')
                ->leftjoin('QuotationDetail','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                ->leftjoin('Branch','Branch.BranchID','=','QuotationDetail.BranchID')
                ->select(['QuotationDetail.BranchID','BranchName','Branch.Phone','Branch.Address','Branch.Contact','DiscountTotalBranch','Branch.Email'])
                ->where('Quotation.QuotationID',$QuotationID)
                ->get();
                $item->branch = $branch;
                $branchcount = count($branch);
                for($j = 0; $j < $branchcount; $j++) {
                    $item = $header[$i]->branch[$j];
                    $BranchID = $header[$i]->branch[$j]->BranchID;
                    $hardware = DB::table('QuotationDetailHardware')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                    ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(['QuotationDetailHardware.HardwareID','HardwareName','QuotationDetailHardware.SubTotal','Quantity',
                    'QuotationDetailHardware.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','DiscountTotal','PriceTotal'])
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->get();
                    $item->hardware = $hardware;
                    $license = DB::table('QuotationDetailLicense')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                    ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(['QuotationDetailLicense.LicenseID','LicenseName','QuotationDetailLicense.SubTotal','QuotationDetailLicense.Quantity','DiscountTotal',
                    'QuotationDetailLicense.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','QuotationDetailLicense.ResellerPrice','SubTotalReseller',
                    'PriceTotal','Free'])
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->get();
                    $item->license = $license;
                    $service = DB::table('QuotationDetailService')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                    ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(['QuotationDetailService.ServiceID','ServiceName','QuotationDetailService.SubTotal','QuotationDetailService.Quantity','DiscountTotal',
                    'QuotationDetailService.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','QuotationDetailService.ResellerPrice','SubTotalReseller',
                    'PriceTotal'])
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->get();
                    $item->service = $service;
                    $licensetotal = DB::table('QuotationDetailLicense')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                    ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->sum('SubTotal');

                    $hardwaretotal = DB::table('QuotationDetailHardware')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                    ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->sum('SubTotal');

                    $servicetotal = DB::table('QuotationDetailService')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                    ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->sum('SubTotal');

                    $branchtotal = $hardwaretotal+$licensetotal+$servicetotal;
                    $item->BranchTotal = $branchtotal;
                    $item->LicenseTotal = $licensetotal;
                    $item->HardwareTotal = $hardwaretotal;
                    $item->ServiceTotal = $servicetotal;
                }
            }
        }
        else{
            $header = DB::table('Quotation')
            ->leftjoin('Brand', 'Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('QuotationDetailUser', 'QuotationDetailUser.QuotationID','=' ,'Quotation.QuotationID')
            ->leftjoin('User','QuotationDetailUser.UserID','=','User.UserID')
            ->leftjoin('Status','Quotation.StatusID','=','Status.StatusID')
            ->select(['Quotation.QuotationID','QuotationNO', 'Quotation.BrandID','BrandName','Brand.Contact','Brand.Email','GrandTotal','GrandTotalReseller','Brand.Address', 'UserFullName','Quotation.StatusID',
                      'StatusName','UserFullName','QuotationDetailUser.UserID','Quotation.QuotationDueDate','Quotation.QuotationDate','Brand.Phone',
                      'GrandTotalDiscount','ProductID'])
            ->where('QuotationDetailUser.UserID',$UserID)
            ->where('Quotation.Archived',null)
            ->get();
            // return $header;
            $headercount = count($header);
            for($i=0;$i<$headercount;$i++)
            {
                $QuotationID = $header[$i]->QuotationID;
                $item = $header[$i];
                $branch = DB::table('Quotation')
                ->leftjoin('Brand', 'Quotation.BrandID','=','Brand.BrandID')
                ->leftjoin('QuotationDetailUser', 'QuotationDetailUser.QuotationID','=' ,'Quotation.QuotationID')
                ->leftjoin('User','QuotationDetailUser.UserID','=','User.UserID')
                ->leftjoin('QuotationDetail','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                ->leftjoin('Branch','Branch.BranchID','=','QuotationDetail.BranchID')
                ->select(['QuotationDetail.BranchID','BranchName','Branch.Phone','Branch.Address','Branch.Contact','DiscountTotalBranch','Branch.Email'])
                ->where('Quotation.QuotationID',$QuotationID)
                ->get();
                $item->branch = $branch;
                $branchcount = count($branch);
                for($j = 0; $j < $branchcount; $j++) {
                    $item = $header[$i]->branch[$j];
                    $BranchID = $header[$i]->branch[$j]->BranchID;
                    $hardware = DB::table('QuotationDetailHardware')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                    ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(['QuotationDetailHardware.HardwareID','HardwareName','QuotationDetailHardware.SubTotal','Quantity',
                    'QuotationDetailHardware.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','DiscountTotal','PriceTotal'])
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->get();
                    $item->hardware = $hardware;
                    $license = DB::table('QuotationDetailLicense')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                    ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(['QuotationDetailLicense.LicenseID','LicenseName','QuotationDetailLicense.SubTotal','QuotationDetailLicense.Quantity','DiscountTotal',
                    'QuotationDetailLicense.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','QuotationDetailLicense.ResellerPrice','SubTotalReseller',
                    'PriceTotal','Free'])
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->get();
                    $item->license = $license;
                    $service = DB::table('QuotationDetailService')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                    ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(['QuotationDetailService.ServiceID','ServiceName','QuotationDetailService.SubTotal','QuotationDetailService.Quantity','DiscountTotal',
                    'QuotationDetailService.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','QuotationDetailService.ResellerPrice','SubTotalReseller',
                    'PriceTotal'])
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->get();
                    $item->service = $service;
                    $licensetotal = DB::table('QuotationDetailLicense')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                    ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->sum('SubTotal');

                    $servicetotal = DB::table('QuotationDetailService')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                    ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.LicenseID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->sum('SubTotal');

                    $hardwaretotal = DB::table('QuotationDetailHardware')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                    ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->sum('SubTotal');

                    $branchtotal = $hardwaretotal+$licensetotal+$servicetotal;
                    $item->ServiceTotal = $servicetotal;
                    $item->BranchTotal = $branchtotal;
                    $item->LicenseTotal = $licensetotal;
                    $item->HardwareTotal = $hardwaretotal;
                }
            }
        }

        // return $header;



      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Quotation' => array(
              'Header' => $header
          )
          );

       return Response()->json($endresult);

    }

    public function getQuotationDetail(request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'QuotationID' => 'required'

      ];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $QuotationID = $input['QuotationID'];
      $header = DB::table('Quotation')
      ->leftjoin('Brand', 'Quotation.BrandID','=','Brand.BrandID')
      ->leftjoin('QuotationDetailUser', 'QuotationDetailUser.QuotationID','=' ,'Quotation.QuotationID')
      ->leftjoin('User','QuotationDetailUser.UserID','=','User.UserID')
      ->leftjoin('Status','Quotation.StatusID','=','Status.StatusID')
      ->select(['Quotation.QuotationID','QuotationNO', 'Quotation.BrandID','BrandName','Brand.Contact','Brand.Email','GrandTotal','GrandTotalReseller','Brand.Address', 'UserFullName','Quotation.StatusID',
                'StatusName','UserFullName','QuotationDetailUser.UserID','Quotation.QuotationDueDate','Quotation.QuotationDate','Brand.Phone',
                'GrandTotalDiscount','ProductID','URL'])
      ->where('Quotation.QuotationID',$QuotationID)
      ->get();

      $branch = DB::table('Quotation')
      ->leftjoin('Brand', 'Quotation.BrandID','=','Brand.BrandID')
      ->leftjoin('QuotationDetailUser', 'QuotationDetailUser.QuotationID','=' ,'Quotation.QuotationID')
      ->leftjoin('User','QuotationDetailUser.UserID','=','User.UserID')
      ->leftjoin('QuotationDetail','Quotation.QuotationID','=','QuotationDetail.QuotationID')
      ->leftjoin('Branch','Branch.BranchID','=','QuotationDetail.BranchID')
      ->select(['QuotationDetail.BranchID','BranchName','Branch.Phone','Branch.Address','Branch.Contact','DiscountTotalBranch','Branch.Email'])
      ->where('Quotation.QuotationID',$QuotationID)
      ->get();

      // $branch = count($branch);
      // return $branch;
      // $branch = array_values(json_decode(json_encode($branch), true));
      for($i = 0, $length = count($branch); $i < $length; $i++) {
          $item = $branch[$i];

          $hardware = DB::table('QuotationDetailHardware')
          ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
          ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
          ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
          ->select(['QuotationDetailHardware.HardwareID','HardwareName','QuotationDetailHardware.SubTotal','Quantity',
          'QuotationDetailHardware.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','DiscountTotal','PriceTotal'])
          ->where('Quotation.QuotationID', $QuotationID)
          ->where('QuotationDetail.BranchID',$item->BranchID)
          ->get();
          $item->hardware = $hardware;
          $license = DB::table('QuotationDetailLicense')
          ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
          ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
          ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
          ->select(['QuotationDetailLicense.LicenseID','LicenseName','QuotationDetailLicense.SubTotal','QuotationDetailLicense.Quantity','DiscountTotal',
          'QuotationDetailLicense.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','QuotationDetailLicense.ResellerPrice','SubTotalReseller',
          'PriceTotal','Free'])
          ->where('Quotation.QuotationID', $QuotationID)
          ->where('QuotationDetail.BranchID',$item->BranchID)
          ->get();
          $item->license = $license;
          $service = DB::table('QuotationDetailService')
          ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
          ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
          ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
          ->select(['QuotationDetailService.ServiceID','ServiceName','QuotationDetailService.SubTotal','QuotationDetailService.Quantity','DiscountTotal',
          'QuotationDetailService.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','QuotationDetailService.ResellerPrice','SubTotalReseller',
          'PriceTotal'])
          ->where('Quotation.QuotationID', $QuotationID)
          ->where('QuotationDetail.BranchID',$item->BranchID)
          ->get();
          $item->service = $service;
          $other = DB::table('QuotationDetailOther')
          ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
          ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
          ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
          ->select(['QuotationDetailOther.OtherID','OtherName','QuotationDetailOther.SubTotal','Quantity',
          'QuotationDetailOther.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','DiscountTotal','PriceTotal'])
          ->where('Quotation.QuotationID', $QuotationID)
          ->where('QuotationDetail.BranchID',$item->BranchID)
          ->get();
          $item->other = $other;
          $licensetotal = DB::table('QuotationDetailLicense')
          ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
          ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
          ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
          ->where('Quotation.QuotationID', $QuotationID)
          ->where('QuotationDetail.BranchID',$item->BranchID)
          ->sum('SubTotal');

          $servicetotal = DB::table('QuotationDetailService')
          ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
          ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
          ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
          ->where('Quotation.QuotationID', $QuotationID)
          ->where('QuotationDetail.BranchID',$item->BranchID)
          ->sum('SubTotal');

          $othertotal = DB::table('QuotationDetailOther')
          ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
          ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
          ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
          ->where('Quotation.QuotationID', $QuotationID)
          ->where('QuotationDetail.BranchID',$item->BranchID)
          ->sum('SubTotal');

          $hardwaretotal = DB::table('QuotationDetailHardware')
          ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
          ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
          ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
          ->where('Quotation.QuotationID', $QuotationID)
          ->where('QuotationDetail.BranchID',$item->BranchID)
          ->sum('SubTotal');

          $branchtotal = $hardwaretotal+$licensetotal+$servicetotal+$othertotal;
          $item->BranchTotal = $branchtotal;
          $item->LicenseTotal = $licensetotal;
          $item->HardwareTotal = $hardwaretotal;
          $item->OtherTotal = $othertotal;
      }
      // return $branch;
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Quotation' => array(
              'Header' => $header, 'Branch'=>$branch
          )
          );

       return Response()->json($endresult);

    }


    public function getQuotationDetailByQuotationID(request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'QuotationID' => 'required',
          'LicenseID' => 'nullable',
          'HardwareID' => 'nullable',
          'DiscountID' => 'nullable'

      ];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $QuotationID = $input['QuotationID'];

      $result = DB::table('QuotationDetail')
      ->select(['QuotationDetail.QuotationID','License.LicenseID','Hardware.HardwareID','QuotationDetail.Price',
      'Discount.DiscountID','Value','Percentage','Qty','SubTotal','SubCommission','User.UserID','User.UserFullName',
      'HardwareName','LicenseName','DiscountName','CommissionValue','CommissionPercentage','QuotationDetail.ResellerPrice','SubTotalReseller'])
      ->leftjoin('Quotation', 'Quotation.QuotationID', '=','QuotationDetail.QuotationID')
      ->leftjoin('SalesVisitation', 'SalesVisitation.SalesVisitationID', '=','Quotation.SalesVisitationID')
      ->leftjoin('License', 'License.LicenseID', '=','QuotationDetail.LicenseID')
      ->leftjoin('Hardware', 'Hardware.HardwareID', '=','QuotationDetail.HardwareID')
      ->leftjoin('Discount', 'Discount.DiscountID', '=','QuotationDetail.DiscountID')
      ->leftjoin('User', 'User.UserID', '=','SalesVisitation.UserID')
      ->where('QuotationDetail.QuotationID',$QuotationID)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'QuotationDetailbyQuotationID' => $result
      );

       return Response()->json($endresult);

    }

public function updateQuotationStatus(request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'QuotationID' => 'required',
      'Status' => 'required'
  ];
  $validator = Validator::make($input, $rules);
     if ($validator->fails()) {
         $errors = $validator->errors();
         $errorList = $this->checkErrors($rules, $errors);
         $additional = null;
         $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
         return response()->json($response);
     }
     $id = $input['QuotationID'];
     $statusID = $input['Status'];
     $result = DB::table('Quotation')
     ->where('QuotationID',$id)
     ->update(array('StatusID'=>$statusID));

     $result = $this->checkReturn($result);
     return Response()->json($result);

}

public function updateQuotationDate(request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'QuotationID' => 'required',
      'QuotationDueDate' => 'required|date|date_format:Y-m-d'
  ];
  $validator = Validator::make($input, $rules);
     if ($validator->fails()) {
         $errors = $validator->errors();
         $errorList = $this->checkErrors($rules, $errors);
         $additional = null;
         $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
         return response()->json($response);
     }
     $id = $input['QuotationID'];
     $QuotationDate = now();
     $param = array(
         'QuotationDate' => $QuotationDate,
         'QuotationDueDate' => $input['QuotationDueDate']
     );
     $result = DB::table('Quotation')
     ->where('QuotationID',$id)
     ->update($param);

     $result = DB::table('Quotation')
     ->where('QuotationID',$id)
     ->update(array('StatusID' => 'PD'));

     $result = $this->checkReturn($result);
     return Response()->json($result);

}

public function InsertUpdateQuotation(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'Data' => 'required'
    ];
 $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $code = 'QUO';
    $count = DB::table('Quotation')
    ->max('QuotationID');
    $count = $count+1;
    $code = $code.$count;

        $BRANDID = $input['Data']['BRAND'];
        $QuotationNO = ($code.$count);
        $GrandTotal = @$input['GrandTotal'];
        $Commission = @$input['Commission'];
        $paramheader = array(
            'QuotationNO' => $code,
            'BrandID' => $BRANDID,
            'QuotationDate' => now()
        );
        //ini input quotation header nya
      $id = @$input["QuotationID"];
      if ($id == null)
      {$result = DB::table('Quotation')
        ->insert($paramheader);
        $id = $this->getLastVal();}
      else
      {$result = DB::table('Quotation')
        ->where('QuotationID', $id)
        ->update(array('GrandTotal' => $GrandTotal,'Commission' => $Commission));}
        // return $paramheader;

        //INSERT DETAIL USER UNTUK QUOTATION NYA
    $result = DB::table('QuotationDetailUser')
    ->insert(array('QuotationID' => $id, 'UserID' => $this->param->UserID));
    if($input['Data']['UserID'] != null)
    {
        $count = count($input['Data']['UserID']);
        for($i = 0; $i<$count; $i++)
        {
            $result = DB::table('QuotationDetailUser')
            ->insert(array('QuotationID' => $id, 'UserID' => $input['Data']['UserID'][$i]));
        }

    }


    //insert detail branch nya apa aja
    for($i = 0; $i < count($input['Data']['BRANCH']); $i++)
    {
        $Branch[] = $input['Data']['BRANCH'][$i]['id'];
        $result = DB::table('QuotationDetail')
        ->insert(array('QuotationID' => $id, 'BranchID' => $Branch[$i]));

    }

    //insert detail Hardware
    for($i = 0; $i < count($input['Data']['BRANCH']); $i++)
    {
        if(count($this->coalesce(@$input['Data']['BRANCH'][$i]['HARDWARE'],[])) > 0){
            for($j = 0; $j < count($input['Data']['BRANCH'][$i]['HARDWARE']);$j++)
            {
                $iddetail = DB::table('QuotationDetail')
                ->where('QuotationID',$id)
                ->where('BranchID', $input['Data']['BRANCH'][$i]['id'])
                ->first()->QuotationDetailID;
                if(@$input['Data']['BRANCH'][$i]['HARDWARE'][$j]['disc_id'] == "")
                {$DiscountID = null;}
                else{$DiscountID = @$input['Data']['BRANCH'][$i]['HARDWARE'][$j]['disc_id'];}
                $Hardware[] = $input['Data']['BRANCH'][$i]['HARDWARE'][$j];
                $result = DB::table('QuotationDetailHardware')
                ->insert(array('QuotationDetailID' =>$iddetail , 'HardwareID' => $input['Data']['BRANCH'][$i]['HARDWARE'][$j]['id'],
                'Price'=>$input['Data']['BRANCH'][$i]['HARDWARE'][$j]['price'], 'DiscountID' => $DiscountID,
                'DiscountValue' => $input['Data']['BRANCH'][$i]['HARDWARE'][$j]['disc_vl'],'DiscountPercentage' => $input['Data']['BRANCH'][$i]['HARDWARE'][$j]['disc_pr'],
                'SubTotal' =>$input['Data']['BRANCH'][$i]['HARDWARE'][$j]['subtotal'], 'DiscountName' => $input['Data']['BRANCH'][$i]['HARDWARE'][$j]['disc_name'],
                'Quantity' =>$input['Data']['BRANCH'][$i]['HARDWARE'][$j]['qty'], 'DiscountTotal' => @$input['Data']['BRANCH'][$i]['HARDWARE'][$j]['disctotal'],
                'PriceTotal' =>$input['Data']['BRANCH'][$i]['HARDWARE'][$j]['pricetotal']));
            }
        }


    }

    //insert detail License
    for($i = 0; $i < count($input['Data']['BRANCH']); $i++)
    {
        if(count($this->coalesce(@$input['Data']['BRANCH'][$i]['LICENSE'],[])) > 0)
        {
            for($j = 0; $j < count($input['Data']['BRANCH'][$i]['LICENSE']);$j++)
            {
                $iddetail = DB::table('QuotationDetail')
                ->where('QuotationID',$id)
                ->where('BranchID', $input['Data']['BRANCH'][$i]['id'])
                ->first()->QuotationDetailID;
                if(@$input['Data']['BRANCH'][$i]['LICENSE'][$j]['disc_id'] == "")
                {$DiscountID = null;}
                else{$DiscountID = @$input['Data']['BRANCH'][$i]['LICENSE'][$j]['disc_id'];}
                $Hardware[] = $input['Data']['BRANCH'][$i]['LICENSE'][$j];
                $result = DB::table('QuotationDetailLicense')
                ->insert(array('QuotationDetailID' =>$iddetail , 'LicenseID' => $input['Data']['BRANCH'][$i]['LICENSE'][$j]['id'],
                'Price'=>$input['Data']['BRANCH'][$i]['LICENSE'][$j]['price'], 'ResellerPrice' => @$input['Data']['BRANCH'][$i]['LICENSE'][$j]['resprice'],
                'DiscountID' => $DiscountID,
                'DiscountValue' => $input['Data']['BRANCH'][$i]['LICENSE'][$j]['disc_vl'],'DiscountPercentage' => $input['Data']['BRANCH'][$i]['LICENSE'][$j]['disc_pr'],
                'SubTotal' =>$input['Data']['BRANCH'][$i]['LICENSE'][$j]['subtotal'],'DiscountName' => $input['Data']['BRANCH'][$i]['LICENSE'][$j]['disc_name'],
                'Quantity' =>$input['Data']['BRANCH'][$i]['LICENSE'][$j]['qty'], 'SubTotalReseller' => @$input['Data']['BRANCH'][$i]['LICENSE'][$j]['reseller_subtotal'],
                'DiscountTotal' => @$input['Data']['BRANCH'][$i]['LICENSE'][$j]['disctotal'], 'PriceTotal' => @$input['Data']['BRANCH'][$i]['LICENSE'][$j]['pricetotal']));
            }
        }


    }

    //buat insert service detail nya
    for($i = 0; $i < count($input['Data']['BRANCH']); $i++)
    {
        if(count($this->coalesce(@$input['Data']['BRANCH'][$i]['SERVICE'],[])) > 0){
            for($j = 0; $j < count($input['Data']['BRANCH'][$i]['SERVICE']);$j++)
            {
                $iddetail = DB::table('QuotationDetail')
                ->where('QuotationID',$id)
                ->where('BranchID', $input['Data']['BRANCH'][$i]['id'])
                ->first()->QuotationDetailID;
                if(@$input['Data']['BRANCH'][$i]['SERVICE'][$j]['disc_id'] == "")
                {$DiscountID = null;}
                else{$DiscountID = @$input['Data']['BRANCH'][$i]['SERVICE'][$j]['disc_id'];}
                $Hardware[] = $input['Data']['BRANCH'][$i]['SERVICE'][$j];
                $result = DB::table('QuotationDetailService')
                ->insert(array('QuotationDetailID' =>$iddetail , 'ServiceID' => $input['Data']['BRANCH'][$i]['SERVICE'][$j]['id'],
                'Price'=>$input['Data']['BRANCH'][$i]['SERVICE'][$j]['price'], 'DiscountID' => $DiscountID,
                'DiscountValue' => $input['Data']['BRANCH'][$i]['SERVICE'][$j]['disc_vl'],'DiscountPercentage' => $input['Data']['BRANCH'][$i]['SERVICE'][$j]['disc_pr'],
                'SubTotal' =>$input['Data']['BRANCH'][$i]['SERVICE'][$j]['subtotal'], 'DiscountName' => $input['Data']['BRANCH'][$i]['SERVICE'][$j]['disc_name'],
                'Quantity' =>$input['Data']['BRANCH'][$i]['SERVICE'][$j]['qty'], 'DiscountTotal' => @$input['Data']['BRANCH'][$i]['SERVICE'][$j]['disctotal'],
                'PriceTotal' =>$input['Data']['BRANCH'][$i]['SERVICE'][$j]['pricetotal']));
            }
        }
    }

    // buat insert other detail nya.
    for($i = 0; $i < count($input['Data']['BRANCH']); $i++)
    {
        if(count($this->coalesce(@$input['Data']['BRANCH'][$i]['OTHER'],[])) > 0){
            for($j = 0; $j < count($input['Data']['BRANCH'][$i]['OTHER']);$j++)
            {
                $iddetail = DB::table('QuotationDetail')
                ->where('QuotationID',$id)
                ->where('BranchID', $input['Data']['BRANCH'][$i]['id'])
                ->first()->QuotationDetailID;
                if(@$input['Data']['BRANCH'][$i]['OTHER'][$j]['disc_id'] == "")
                {$DiscountID = null;}
                else{$DiscountID = @$input['Data']['BRANCH'][$i]['OTHER'][$j]['disc_id'];}
                $Hardware[] = $input['Data']['BRANCH'][$i]['OTHER'][$j];
                $result = DB::table('QuotationDetailOther')
                ->insert(array('QuotationDetailID' =>$iddetail , 'OtherID' => $input['Data']['BRANCH'][$i]['OTHER'][$j]['id'],
                'Price'=>$input['Data']['BRANCH'][$i]['OTHER'][$j]['price'], 'DiscountID' => $DiscountID,
                'DiscountValue' => $input['Data']['BRANCH'][$i]['OTHER'][$j]['disc_vl'],'DiscountPercentage' => $input['Data']['BRANCH'][$i]['OTHER'][$j]['disc_pr'],
                'SubTotal' =>$input['Data']['BRANCH'][$i]['OTHER'][$j]['subtotal'], 'DiscountName' => $input['Data']['BRANCH'][$i]['OTHER'][$j]['disc_name'],
                'Quantity' =>$input['Data']['BRANCH'][$i]['OTHER'][$j]['qty'], 'DiscountTotal' => @$input['Data']['BRANCH'][$i]['OTHER'][$j]['disctotal'],
                'PriceTotal' =>$input['Data']['BRANCH'][$i]['OTHER'][$j]['pricetotal']));
            }
        }
    }

    for($i = 0; $i < count($input['Data']['BRANCH']); $i++)
    {
        $iddetail = DB::table('QuotationDetail')
        ->where('QuotationID',$id)
        ->where('BranchID', $input['Data']['BRANCH'][$i]['id'])
        ->first()->QuotationDetailID;
        $HardwareTotal = DB::table('QuotationDetailHardware')
        ->where('QuotationDetailID', $iddetail)
        ->sum('SubTotal');
        $LicenseTotal = DB::table('QuotationDetailLicense')
        ->where('QuotationDetailID', $iddetail)
        ->sum('SubTotal');
        $ServiceTotal = DB::table('QuotationDetailService')
        ->where('QuotationDetailID', $iddetail)
        ->sum('SubTotal');
        $OtherTotal = DB::table('QuotationDetailOther')
        ->where('QuotationDetailID', $iddetail)
        ->sum('SubTotal');
        $LicenseTotalReseller = DB::table('QuotationDetailLicense')
        ->where('QuotationDetailID', $iddetail)
        ->sum('ResellerPrice');
        $ServiceTotalReseller = DB::table('QuotationDetailService')
        ->where('QuotationDetailID', $iddetail)
        ->sum('ResellerPrice');

        $Total = $LicenseTotal + $HardwareTotal+$ServiceTotal+$OtherTotal;
        $TotalReseller = $LicenseTotalReseller + $HardwareTotal+$ServiceTotalReseller+$OtherTotal;
        $subtotal = DB::table('QuotationDetail')
        ->where('QuotationDetailID', $iddetail)
        ->update(array('Total' => $Total));
        $subtotalreseller = DB::table('QuotationDetail')
        ->where('QuotationDetailID', $iddetail)
        ->update(array('TotalReseller' => $TotalReseller));
        $hardwarediscount = DB::table('QuotationDetailHardware')
        ->where('QuotationDetailID',$iddetail)
        ->sum('DiscountTotal');
        $licensediscount = DB::table('QuotationDetailLicense')
        ->where('QuotationDetailID',$iddetail)
        ->sum('DiscountTotal');
        $servicediscount = DB::table('QuotationDetailService')
        ->where('QuotationDetailID',$iddetail)
        ->sum('DiscountTotal');
        $otherdiscount = DB::table('QuotationDetailOther')
        ->where('QuotationDetailID',$iddetail)
        ->sum('DiscountTotal');
        $DiscountTotalBranch = $hardwarediscount+$licensediscount+$servicediscount+$otherdiscount;
        $discountbranch = DB::table('QuotationDetail')
        ->where('QuotationDetailID',$iddetail)
        ->update(array('DiscountTotalBranch' => $DiscountTotalBranch));
    }
    $GrandTotal = DB::table('QuotationDetail')
    ->where('QuotationID',$id)
    ->sum('Total');
    $GrandTotalDiscount = DB::table('QuotationDetail')
    ->where('QuotationID',$id)
    ->sum('DiscountTotalBranch');
    $GrandTotalReseller = DB::table('QuotationDetail')
    ->where('QuotationID',$id)
    ->sum('TotalReseller');
    if($GrandTotalDiscount == 0)
    {$statusID = "A";}
    else{
        $statusID = "W";
    }

    $result = DB::table('Quotation')
    ->where('QuotationID',$id)
    ->update(array('GrandTotal' => $GrandTotal, 'GrandTotalReseller' => $GrandTotalReseller,'StatusID' => $statusID,
    'GrandTotalDiscount' => $GrandTotalDiscount));

    $result = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'QuotationID' => $id);

    return Response()->json($result);

  }
      public function insertUpdateQuotationDetail(request $request){
        $input = json_decode($request->getContent(), true);
        $rules = [
            'QuotationID' => 'required',
            'LicenseID' => 'nullable|distinct|array',
            'HardwareID' => 'distinct|nullable|array',
            'Price' => 'required|array',
            'Value' => 'array|nullable',
            'Qty' => 'required|array',
            'DiscountID' =>'array|nullable',
            'Percentage' => 'array|nullable',
            'SubTotal' => 'array|nullable',
            'SubCommission' => 'array|nullable',
            'CommissionPercentage' => 'array|nullable',
            'CommissionValue' => 'array|nullable',
            'SubTotalReseller' => 'array|nullalbe',
            'ResellerPrice' => 'array|nullable'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $Status = @$input['Status'];

          $QuotationID = $input['QuotationID'];
          $result = DB::table('QuotationDetail')
          ->select('QuotationID')
          ->where('QuotationID',$QuotationID)
          ->get();

          if($result == null){
              $temphard = 'HardwareID';
              $templice = 'LicenseID';
              $pricetemp = 'Price';
              $resellerpricetemp = 'ResellerPrice';
              $qtytemp = 'Qty';
              $disctemp = 'DiscountID';
              $perctemp = 'Percentage';
              $valuetemp = 'Value';
              $subtemp = 'SubTotal';
              $subcommtemp = 'SubCommission';
              $commperctemp = 'CommissionPercentage';
              $commvaluetemp = 'CommissionValue';
              $subresellertemp = 'SubTotalReseller';

        for($i = 0; $i < count($input[$temphard]);$i++){
            $itemshard = $input[$temphard][$i];
            $itemslice = $input[$templice][$i];
            $price = $input[$pricetemp][$i];
            $qty = $input[$qtytemp][$i];
            $disc = @$input[$disctemp][$i];
            $perc = @$input[$perctemp][$i];
            $value = @$input[$valuetemp][$i];
            $resellerprice = @$input[$resellerpricetemp][$i];
            if ($disc == null)
            {$sub = $price*$qty;
            $subreseller = $resellerprice*$qty;}
            elseif ($perc == null) {
              $sub = ($price*$qty-$value);
              if($resellerprice == 0)
              {$subreseller = 0;}
              else{$subreseller = ($resellerprice*$qty-$value);}}
            else {
              $sub = $price*$qty-($perc/100*($price*$qty));
              if($resellerprice == 0)
              {$subreseller = 0;}
              else{$subreseller = $resellerprice*$qty-($perc/100*($resellerprice*$qty));}

            }
            $commperc = @$input[$commperctemp][$i];
            $commvalue = @$input[$commvaluetemp][$i];
            if ($commperc == null){
              $subcomm = $commvalue;
            }
            else{
              $subcomm = ($commperc/100*$sub);
            }
            $insertDetail = array(
            'QuotationID' => $QuotationID,
              $temphard => @$itemshard,
              $templice => @$itemslice,
              $pricetemp =>@$price,
              $qtytemp => @$qty,
              $disctemp => @$disc,
              $perctemp => @$perc,
              $valuetemp => @$value,
              $subtemp => @$sub,
              $subcommtemp => @$subcomm,
              $subresellertemp => @$subreseller,
              $resellerpricetemp => @$resellerprice
            );
              $resultDetail = DB::table('QuotationDetail')->insert($insertDetail);
              $GrandTotal = DB::table('QuotationDetail')
              ->where('QuotationID', $QuotationID)
              ->sum('SubTotal');

              $Commission = DB::table('QuotationDetail')
              ->where('QuotationID', $QuotationID)
              ->sum('SubCommission');

              $GrandTotalReseller = DB::table('QuotationDetail')
              ->where('QuotationID',$QuotationID)
              ->sum('SubTotalReseller');

              $result = DB::table('Quotation')
              ->where('QuotationID', $QuotationID)
              ->update(array('GrandTotal' => $GrandTotal,'Commission' => $Commission, 'GrandTotalReseller' => $GrandTotalReseller));

              $result = DB::table('Quotation')
              ->where('QuotationID',$QuotationID)
              ->update(array('Status' => $Status));
        }
      }

else{ $resultDetail = DB::table('QuotationDetail')
  ->where('QuotationID',$QuotationID)
  ->delete();
  $temphard = 'HardwareID';
  $templice = 'LicenseID';
  $pricetemp = 'Price';
  $resellerpricetemp = 'ResellerPrice';
  $qtytemp = 'Qty';
  $disctemp = 'DiscountID';
  $perctemp = 'Percentage';
  $valuetemp = 'Value';
  $subtemp = 'SubTotal';
  $subcommtemp = 'SubCommission';
  $commperctemp = 'CommissionPercentage';
  $commvaluetemp = 'CommissionValue';
  $subresellertemp = 'SubTotalReseller';




for($i = 0; $i < count($input[$temphard]);$i++){
$itemshard = $input[$temphard][$i];
$itemslice = $input[$templice][$i];
$price = $input[$pricetemp][$i];
$qty = $input[$qtytemp][$i];
$disc = @$input[$disctemp][$i];
$perc = @$input[$perctemp][$i];
$value = @$input[$valuetemp][$i];
$resellerprice = @$input[$resellerpricetemp][$i];
if ($disc == null)
{$sub = $price*$qty;
$subreseller = $resellerprice*$qty;}
elseif ($perc == null) {
  $sub = ($price*$qty-$value);
  if($resellerprice == 0)
  {$subreseller = 0;}
  else{$subreseller = ($resellerprice*$qty-$value);}}
else {
  $sub = $price*$qty-($perc/100*($price*$qty));
  if($resellerprice == 0)
  {$subreseller = 0;}
  else{$subreseller = $resellerprice*$qty-($perc/100*($resellerprice*$qty));}

}
$commperc = @$input[$commperctemp][$i];
$commvalue = @$input[$commvaluetemp][$i];
if ($commperc == 0){
  $subcomm = $commvalue;
}
else{
  $subcomm = ($commperc/100*$sub);
}

$insertDetail = array(
'QuotationID' => $QuotationID,
  $temphard => @$itemshard,
  $templice => @$itemslice,
  $pricetemp =>@$price,
  $qtytemp => @$qty,
  $disctemp => @$disc,
  $perctemp => @$perc,
  $valuetemp => @$value,
  $subtemp => @$sub,
  $subcommtemp => @$subcomm,
  $subresellertemp => @$subreseller,
  $resellerpricetemp => @$resellerprice
);

$resultDetail = DB::table('QuotationDetail')->insert($insertDetail);
$GrandTotal = DB::table('QuotationDetail')
->where('QuotationID', $QuotationID)
->sum('SubTotal');

$Commission = DB::table('QuotationDetail')
->where('QuotationID', $QuotationID)
->sum('SubCommission');

$GrandTotalReseller = DB::table('QuotationDetail')
->where('QuotationID',$QuotationID)
->sum('SubTotalReseller');

$result = DB::table('Quotation')
->where('QuotationID', $QuotationID)
->update(array('GrandTotal' => $GrandTotal,'Commission' => $Commission, 'GrandTotalReseller' => $GrandTotalReseller));

$result = DB::table('Quotation')
->where('QuotationID',$QuotationID)
->update(array('Status' => $Status));
}

    }
    $result = $this->checkReturn($resultDetail);

    return Response()->json($result);
}




     public function DeleteQuotation(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $QuotationID = @$input['QuotationID'];
       $result = DB::table('Quotation')->where('QuotationID', $QuotationID)->update(array(
                  'Archived' => now(),
           ));

      $result = $this->checkReturn($result);

      return Response()->json($result);
    }
}
