<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class InvoicePaymentController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getInvoicePayment(){
      $result = DB::table('InvoicePayment')
      ->leftjoin('Invoice','Invoice.InvoiceID','=','InvoicePayment.InvoiceID')
      ->select(['InvoicePayment.InvoiceID','Date','InvoiceCode','Paid','TransferProof','Status'])
      ->get();

       return Response()->json($result);
    }

    public function getInvoicePaymentDetail(){
      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'InvoicePaymentID' => 'required'
      ];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $InvoicePaymentID = $input['InvoicePaymentID'];
      $result = DB::table('InvoicePayment')
      ->leftjoin('Invoice','Invoice.ProformaID','=','InvoicePayment.ProformaID')
      ->select(['InvoicePaymentID','InvoicePayment.ProformaID','ProformaNO','Date','InvoiceNO','Paid','TransferProof','InvoicePayment.Status'])
      ->where('InvoicePaymentID',$InvoicePaymentID)
      ->get();
      $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'InvoicePaymentDetail' => $result
        );
        return Response()->json($endresult);
    }

    public function getInvoicePaymentDetailByInvoiceID(Request $request){

      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'ProformaID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $ProformaID = $input['ProformaID'];
      $result = DB::table('InvoicePayment')
      ->leftjoin('Invoice','Invoice.ProformaID','=','InvoicePayment.ProformaID')
      ->select(['InvoicePaymentID','InvoicePayment.ProformaID','Date','InvoiceNO','Paid','TransferProof','InvoicePayment.Status'])
      ->where('InvoicePayment.ProformaID',$ProformaID)
      ->get();


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'InvoicePayment' => $result);
      return Response()->json($endresult);

}

public function insertUpdateInvoicePayment(request $request){
  $input = json_decode($request->getContent(), true);
  $rules = [
      'ProformaID' => 'required',
      'Date' => 'date|date_format:Y-m-d|nullable',
      'Paid' => 'required'
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);}

  $param = array (
    'ProformaID' => $input['ProformaID'],
    'Date' => @$input['Date'],
    'Paid' => $input['Paid'],
    'Status' => @$input['Status']
  );


  $ID = @$input['InvoicePaymentID'];
  if(@$input['DeleteImage'] === true){$param = ["Image" => null];}
        $ProformaID = @$input['ProformaID'];
          if($ID == null){
            $result = DB::table('InvoicePayment')->insert($param);
          $ID = $this->getLastVal();
          }
          else{
            $result = DB::table('InvoicePayment')
            ->where('InvoicePaymentID',$ID)
            ->update($param);
          }

          if(@$input['TransferProof'] !== null){
                            $arr = array(
                                'UserID' => $this->param->UserID,
                                'ObjectID' => $ID,
                                'Folder' => 'Brand',
                                'Filename' => @$input['TransferProof']['Filename'],
                                'Data' =>  @$input['TransferProof']['Data']
                            );
                            $path = $this->upload_to_s3($arr);
                            $data = ['TransferProof' => $path];
                            $result = DB::table('InvoicePayment')->where('InvoicePaymentID',$ID)->update($data);

                            $response = $this->generateResponse(0, [], "Success", ['Brand'=>$result]);
                        }
        $GrandTotal = DB::table('Invoice')
        ->leftjoin('Quotation','Quotation.QuotationID','=','Invoice.QuotationID')
        ->select(['Invoice.GrandTotal'])
        ->where('ProformaID',$ProformaID)
        ->first()->GrandTotal;

        $TotalPayment = DB::table('InvoicePayment')
        ->where('ProformaID',$ProformaID)
        ->sum('Paid');


        if($TotalPayment == $GrandTotal)
        {$result = DB::table('Invoice')
        ->where('ProformaID',$ProformaID)
        ->update(array('StatusID' => "P2", 'PaidDate' => now()));
        $InvoiceID = DB::table('Invoice')
        ->where('ProformaID',$ProformaID)
        ->first()->InvoiceID;
        if($InvoiceID === null)
        {
            $year = date('Y');
            $month = date('m');
            $num = DB::table('Invoice')
            ->whereraw('CAST(SUBSTRING("InvoiceNO", 8,4) AS INTEGER)  ='.$year.' and CAST(SUBSTRING("InvoiceNO", 13,2) AS INTEGER) ='.$month)
            ->max('InvoiceID');
            $num = $num+1;
                $count = str_pad($num, 6, '0', STR_PAD_LEFT);
                $code = 'HB-INV-'.$year.'-'.$month.($count);
                $InvoiceNO = $code;

                $yearreceipt = substr($input['Date'],0,4);
                $monthreceipt = substr($input['Date'],5,2);
                $numreceipt = DB::table('Invoice')
                ->whereraw('CAST(SUBSTRING("ReceiptNO", 8,4) AS INTEGER)  ='.$yearreceipt.' and CAST(SUBSTRIreceipteiptNO", 13,2) AS INTEGER) ='.$monthreceipt)
                ->max('ReceiptID');
                $numreceipt = $numreceipt+1;
                    $countreceipt = str_pad($numreceipt, 6, '0', STR_PAD_LEFT);
                    $codereceipt = 'HB-REC-'.$yearreceipt.'-'.$monthreceipt.($countreceipt);
                    $ReceiptNO = $codereceipt;
                    $result = DB::table('Invoice')
                    ->where('ProformaID',$ProformaID)
                    ->update(array('InvoiceNO' => $InvoiceNO, 'ReceiptNO' => $ReceiptNO, 'ReceiptID' => $numreceipt,'InvoiceID' => $num));

        }
        else{
            $year = date('Y');
            $month = date('m');
            $yearreceipt = substr($input['Date'],0,4);
            $monthreceipt = substr($input['Date'],5,2);
            $numreceipt = DB::table('Invoice')
            ->whereraw('CAST(SUBSTRING("ReceiptNO", 8,4) AS INTEGER)  ='.$yearreceipt.' and CAST(SUBSTRIreceipteiptNO", 13,2) AS INTEGER) ='.$monthreceipt)
            ->max('ReceiptID');

            $numreceipt = $numreceipt+1;
                $countreceipt = str_pad($numreceipt, 6, '0', STR_PAD_LEFT);
                $codereceipt = 'HB-REC-'.$yearreceipt.'-'.$monthreceipt.($countreceipt);
                $ReceiptNO = $codereceipt;
                $result = DB::table('Invoice')
                ->where('ProformaID',$ProformaID)
                ->update(array('ReceiptNO' => $ReceiptNO, 'ReceiptID' => $numreceipt));
        }


        }
        else
        {$result = DB::table('Invoice')
        ->where('ProformaID',$ProformaID)
        ->update(array('StatusID' => "P", 'PaidDate' => now()));
        }

        if(@$input['Period'] != null)
        {
            $period = $input['Period'];
            for($i=0;$i<$countperiod;$i++){
                $periodnew = $period[$i];
                $startdate = DB::table('Invoice')
                ->leftjoin('Quotation','Quotation.QuotationID','=','Invoice.QuotationID')
                ->leftjoin('QuotationDetail','QuotationDetail.QuotationID','=','Quotation.QuotationID')
                ->leftjoin('QuotationDetailLicense','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                ->where('QuotationDetail.BranchID',$periodnew['branch_id'])
                ->where('Invoice.ProformaID',$ProformaID)
                ->select(['StartDate','EndDate','QuotationDetailLicenseID'])
                ->get();
                $start = $startdate[0]->StartDate;

                if($start === null){
                        DB::table('QuotationDetailLicense')
                        ->where('QuotationDetailLicenseID',$startdate[0]->QuotationDetailLicenseID)
                        ->update(array('StartDate' => $periodnew['start_date'],'EndDate' => $periodnew['end_date']));
                }

            }

        }

              $result = $this->checkReturn($result);
              return Response()->json($result);
}
}
