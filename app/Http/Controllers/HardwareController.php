<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class HardwareController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getHardware(Request $request){
        $input = json_decode($request->getContent(),true);

        $isGetAll = @$input['isGetAllHardware'];
        if($isGetAll == true)
        {
            $result = DB::table('Hardware')->select(['HardwareID','HardwareCode','HardwareName','Price','Type', 'Description'])
            ->where('Archived',null)
            ->orderby('HardwareID','asc')
            ->get();
        }
        else{
            $result = DB::table('Hardware')->select(['HardwareID','HardwareCode','HardwareName','Price','Type', 'Description'])
            ->where('Archived',null)
            ->where('Description',null)
            ->orderby('HardwareID','asc')
            ->get();
        }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Hardware' => $result
      );
    return Response()->json($endresult);
    }

    public function getHardwareDetail(request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'HardwareID' => 'required',
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $HardwareID = $input['HardwareID'];
      $result = DB::table('Hardware')
      ->select(['HardwareID','HardwareCode','HardwareName','Price','Type','Description'])
      ->where('HardwareID',$HardwareID)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Hardware' => $result
      );
    return Response()->json($endresult);

    }

    public function InsertUpdateHardware(request $request){

        $input = json_decode($request->getContent(), true);
        $rules = [
        'HardwareCode' => 'required',
        'HardwareName' => 'required',
        'Price' => 'required',
        'Type' => 'required',
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $ID = @$input['HardwareID'];
        $unique = array(
            'Table' => "Hardware",
            'ID' => $ID,
            'Column' => "HardwareName",
            'String' => $input['HardwareName']
        );
        $uniqueHardwareName = $this->unique($unique);
        $unique['Column'] = "HardwareCode";
        $unique['String'] = $input['HardwareCode'];
        $uniqueHardwareCode = $this->unique($unique);

        $HardwareCode = $input['HardwareCode'];
        $HardwareName = $input['HardwareName'];
        $Price = $input['Price'];
        $Type = $input['Type'];
        $Description = @$input['Description'];

        if($ID == null){$result = DB::table('Hardware')
        ->insert(array (
        'HardwareCode' => $HardwareCode,
        'HardwareName' => @$input['HardwareName'],
        'Price' => $input['Price'],
        'Type' => $input['Type'],
        'Description' => @$input['Description']));}
        else {$result = DB::table('Hardware')
          ->where('HardwareID', $ID)
          ->update(array (
          'HardwareName' => @$input['HardwareName'],
          'Price' => $input['Price'],
          'Type' => $input['Type'],
          'Description' => @$input['Description']));}

        $result = $this->checkReturn($result);

        return Response()->json($result);

      }
      public function ActivateDeactivateHardware(Request $request){
           $input = json_decode($this->request->getContent(),true);
           $rules = ['HardwareID' => 'required'];
           $validator = Validator::make($input, $rules);
           if ($validator->fails()) {
               $errors = $validator->errors();
               $errorList = $this->checkErrors($rules, $errors);
               $additional = null;
               $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
               return response()->json($response);
           }
           $HardwareID = @$input['HardwareID'];

           $HardwareStatus = DB::table('Hardware')
           ->where('HardwareID',$HardwareID)
           ->first()->Description;

           if($HardwareStatus == null)
           {
               $param = array('Description' => 'Deactivated');
               $result = DB::table('Hardware')->where('HardwareID', $HardwareID)->update($param);
           }
           else{
               $param = array('Description' => null);
               $result = DB::table('Hardware')->where('HardwareID', $HardwareID)->update($param);
           }


          $result = $this->checkReturn($result);

          return Response()->json($result);

      }
      public function DeleteHardware(Request $request){
           $input = json_decode($this->request->getContent(),true);
           $rules = ['HardwareID' => 'required'];
           $validator = Validator::make($input, $rules);
           if ($validator->fails()) {
               $errors = $validator->errors();
               $errorList = $this->checkErrors($rules, $errors);
               $additional = null;
               $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
               return response()->json($response);
           }
           $HardwareID = @$input['HardwareID'];
           $param = array('Status' => 'D','Archived' => now());

           $result = DB::table('Hardware')->where('HardwareID', $HardwareID)->update($param);

          $result = $this->checkReturn($result);

          return Response()->json($result);

      }
}
