<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use App\Mail\SetPassword;
use Mail;

class UserController extends Controller
{
        public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }


    public function getUserPassedPicTrue(){
      $result = DB::table('User')
      ->leftjoin('UserType', 'User.UserTypeID', '=','UserType.UserTypeID')
      ->select(['UserID','UserCode','UserFullName','Username','User.UserTypeID','UserTypeName','Email','Phone','Address','PassedPIC'])
      ->where('User.Status',null)
      ->where('UserType.PassedPIC',TRUE)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'User' => $result
      );

       return Response()->json($endresult);
    }

    public function getUser(){
      $result = DB::table('User')
      ->leftjoin('UserType', 'User.UserTypeID', '=','UserType.UserTypeID')
      ->select(['UserID','UserCode','UserFullName','Username','User.UserTypeID','UserTypeName','Email','Phone','Address'])
      ->where('User.Status',null)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'User' => $result
      );

       return Response()->json($endresult);
    }

    public function getUserDetail(request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'UserID' => 'required'
      ];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $UserID = $input['UserID'];

      $result = DB::table('User')
      ->join('UserType', 'User.UserTypeID', '=','UserType.UserTypeID')
      ->select(['UserID','UserCode','UserFullName','Username','User.UserTypeID','UserTypeName','Email','Phone','Address'])
      ->where('UserID',$UserID)
      ->get();
         return Response()->json($result);

    }

    public function InsertUpdateUser(Request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'UserCode' => 'required',
            'UserFullName' => 'required',
            'UserTypeID' => 'required',
            'Email' => 'required|email',
            'Phone' => 'required|numeric',
            'Address' => 'required',
            'Username' => 'required',
            'Password' => 'min:8|regex:/^.*(?=.{3,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$/'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $ID = @$input["UserID"];
        $unique = array(
            'Table' => "User",
            'ID' => $ID,
            'Column' => "Username",
            'String' => $input['Username']
        );
        $uniqueUsername = $this->unique($unique);
        $unique['Column'] = "UserCode";
        $unique['String'] = $input['UserCode'];
        $uniqueUserCode = $this->unique($unique);
        $unique['Column'] = "Email";
        $unique['String'] = $input['Email'];
        $uniqueEmail = $this->unique($unique);

        $param = array(
            'UserCode' => @$input['UserCode'],
            'UserFullName' => $input['UserFullName'],
            'UserTypeID' => $input['UserTypeID'],
            'JoinDate' => @$input['JoinDate'],
            'Username' => $input['Username'],
            'Password' => password_hash(@$input['Password'], PASSWORD_BCRYPT),
            'Email' => $input['Email'],
            'Address' => @$input['Address'],
            'Phone' => @$input['Phone'],
            'BankAccount' => @$input['BankAccount']
          );
          $param2 = array(
              'UserCode' => @$input['UserCode'],
              'UserFullName' => $input['UserFullName'],
              'UserTypeID' => $input['UserTypeID'],
              'JoinDate' => @$input['JoinDate'],
              'Username' => $input['Username'],
              'Email' => $input['Email'],
              'Address' => @$input['Address'],
              'Phone' => @$input['Phone'],
              'BankAccount' => @$input['BankAccount']
            );

          if ($ID == null)
            {$result = DB::table('User')->insert($param);
            $ID = $this->getLastVal();}
          else
          {$result = DB::table('User')->where('UserID', $ID)->update($param2);}
        //   $objDemo = new \stdClass();
        //   $UserFullName = $param['UserFullName'];
        // $objDemo->link = 'Hellobill.com/set_password';
        // $objDemo->sender = 'HelloBill';
        // $objDemo->receiver = $UserFullName;
        //
        //  Mail::to($input['Email'])->send(new SetPassword($objDemo));
        $result = $this->checkReturn($result);
        return Response()->json($result);
      }




     public function DeleteUser(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $UserID = @$input['UserID'];
       $result = DB::table('User')->where('UserID', $UserID)->update(array(
                  'Status' => 'D','Archived' => now()
           ));

      $result = $this->checkReturn($result);

      return Response()->json($result);

    }


public function changePassword(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'OldPassword' => 'required',
        'NewPassword' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$/'
    ];
    $messages = array(
                    'regex' => 'Passwords Must Contain Uppercase, Lowercase, And Numeric'
                );
                $validator = Validator::make($input, $rules, $messages);
                    if ($validator->fails()) {
                        // validation error
                        $errors = $validator->errors();
                        $errorList = $this->checkErrors($rules, $errors);
                        $additional = null;
                        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
                        return response()->json($response);
                    }
    $NewPassword = password_hash($input['NewPassword'], PASSWORD_BCRYPT);

    $password = collect(\DB::select('select "Password", "UserID", "UserTypeID"
                            from "User"
                            where "Archived" is null
                            AND ("UserID" = :userid)',
                            array(
                                'userid' => $this->param->UserID,

                            )))->first();

    if(password_verify(@$input['OldPassword'], $password->Password))
    {
        $param = array(
            'Password' => $NewPassword
        );
        $result = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->update($param);

        $result = $this->checkReturn($result);
    }
    else{
        $result = array(
            'Status' => 1,
            'Errors' => array(
                array(
                    'ID' => "Password",'Message'=>"Wrong Password")
            ),
            'Message' => "Fail"
        );
    }

return Response()->json($result);
}



}
