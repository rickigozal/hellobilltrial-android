<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Validator;

class InvoiceController extends Controller
{
        public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }


    public function getInvoice(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'isGetAll' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }

        $UserID = $this->param->UserID;
        $isGetAll = $input['isGetAll'];

        if($isGetAll === true){
            $result = DB::table('Invoice')
            ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
            ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('QuotationDetailUser','Quotation.QuotationID','=','QuotationDetailUser.QuotationID')
            ->leftjoin('User', 'QuotationDetailUser.UserID','=','User.UserID')
            ->leftjoin('UserType','UserType.UserTypeID','=','User.UserTypeID')
            ->leftjoin('Status', 'Invoice.StatusID','=','Status.StatusID')
            ->leftjoin('Branch','Branch.BranchID','=','Invoice.BranchID')
            ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO','UserFullName',
            'QuotationDetailUser.UserID','Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName',
            'StatusName','DeliveryDate','DueDate','PaidDate', 'User.UserTypeID','UserTypeName','Brand.Contact','Brand.Phone', 'Brand.Email','Brand.Address',
            'Invoice.GrandTotalDiscount','Invoice.URL','ProformaID','ProformaNO','ReceiptID','ReceiptNO','PONumber'])
            ->orderby('ProformaID','desc')
            ->get();
        }
        else{
            $result = DB::table('Invoice')
            ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
            ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('QuotationDetailUser','Quotation.QuotationID','=','QuotationDetailUser.QuotationID')
            ->leftjoin('User', 'QuotationDetailUser.UserID','=','User.UserID')
            ->leftjoin('UserType','UserType.UserTypeID','=','User.UserTypeID')
            ->leftjoin('Status', 'Invoice.StatusID','=','Status.StatusID')
            ->leftjoin('Branch','Branch.BranchID','=','Invoice.BranchID')
            ->where('QuotationDetailUser.UserID',$UserID)
            ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO','UserFullName',
            'QuotationDetailUser.UserID','Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName',
            'StatusName','DeliveryDate','DueDate','PaidDate', 'User.UserTypeID','UserTypeName','Brand.Contact','Brand.Phone', 'Brand.Email','Brand.Address',
            'Invoice.GrandTotalDiscount','Invoice.URL','ProformaID','ProformaNO','ReceiptID','ReceiptNO','PONumber'])
            ->orderby('ProformaID','desc')
            ->get();
        }


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Invoice' => $result
      );

       return Response()->json($endresult);

    }

    public function generateInvoiceID(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'ProformaID' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $ProformaID = $input['ProformaID'];
        $UserID = $this->param->UserID;

        $InvoiceID = DB::table('Invoice')
        ->where('ProformaID',$ProformaID)
        ->select(['InvoiceID'])
        ->first()->InvoiceID;

        if($InvoiceID === null)
        {
            $year = date('Y');
            $month = date('m');
            $num = DB::table('Invoice')
            ->whereraw('CAST(SUBSTRING("InvoiceNO", 8,4) AS INTEGER)  ='.$year.' and CAST(SUBSTRING("InvoiceNO", 13,2) AS INTEGER) ='.$month)
            ->max('InvoiceID');
            $num = $num+1;
                $count = str_pad($num, 6, '0', STR_PAD_LEFT);
                $code = 'HB-INV-'.$year.'-'.$month.($count);
                $InvoiceNO = $code;
                $result = DB::table('Invoice')
                ->where('ProformaID',$ProformaID)
                ->update(array('InvoiceNO' => $InvoiceNO,'InvoiceID' => $num));
        }
        else{
            $result = "InvoiceID already generated.";
            }

            if(@$input['Period'] != null)
            {
                $countperiod = count(@$input['Period']);
                $period = @$input['Period'];
                for($i=0;$i<$countperiod;$i++){
                    $periodnew = $period[$i];
                    $startdate = DB::table('Invoice')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','Invoice.QuotationID')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationID','=','Quotation.QuotationID')
                    ->leftjoin('QuotationDetailLicense','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                    ->where('QuotationDetail.BranchID',$periodnew['branch_id'])
                    ->where('Invoice.ProformaID',$ProformaID)
                    ->select(['StartDate','EndDate','QuotationDetailLicenseID'])
                    ->get();

                    $start = $startdate[0]->StartDate;

                    if($start === null){
                            DB::table('QuotationDetailLicense')
                            ->where('QuotationDetailLicenseID',$startdate[0]->QuotationDetailLicenseID)
                            ->update(array('StartDate' => $periodnew['start_date'],'EndDate' => $periodnew['end_date']));
                    }
            }


            }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Invoice' => $result
      );

       return Response()->json($endresult);

    }

    public function getInvoiceDetail(request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'ProformaID' => 'required'
      ];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $ProformaID = $input['ProformaID'];
      // return $InvoiceID;
      $result = DB::table('Invoice')
      ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
      ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
      ->leftjoin('QuotationDetailUser','Quotation.QuotationID','=','QuotationDetailUser.QuotationID')
      ->leftjoin('User', 'QuotationDetailUser.UserID','=','User.UserID')
      ->leftjoin('UserType','UserType.UserTypeID','=','User.UserTypeID')
      ->leftjoin('Status','Invoice.StatusID','=','Status.StatusID')
      ->leftjoin('Branch','Invoice.BranchID','Branch.BranchID')
      ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO','UserFullName','ProformaID','ProformaNO',
      'QuotationDetailUser.UserID','Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName',
      'StatusName','DeliveryDate','DueDate','PaidDate', 'User.UserTypeID','UserTypeName','Brand.Contact','Brand.Phone', 'Brand.Email','Brand.Address',
      'Invoice.GrandTotalDiscount','Invoice.URL','ProformaNO','ReceiptID','ReceiptNO','PONumber','Note'])
      ->where('ProformaID',$ProformaID)
      ->get();
      $result2 = DB::table('Invoice')
      ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
      ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
      ->leftjoin('QuotationDetailUser','Quotation.QuotationID','=','QuotationDetailUser.QuotationID')
      ->leftjoin('User', 'QuotationDetailUser.UserID','=','User.UserID')
      ->leftjoin('UserType','UserType.UserTypeID','=','User.UserTypeID')
      ->leftjoin('Status','Invoice.StatusID','=','Status.StatusID')
      ->leftjoin('Branch','Invoice.BranchID','Branch.BranchID')
      ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO','UserFullName','ProformaID','ProformaNO',
      'QuotationDetailUser.UserID','Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName',
      'StatusName','DeliveryDate','DueDate','PaidDate', 'User.UserTypeID','UserTypeName','Branch.Contact','Branch.Phone', 'Branch.Email','Branch.Address',
      'Invoice.GrandTotalDiscount','Invoice.URL','ProformaNO','ReceiptID','ReceiptNO','PONumber','Note'])
      ->where('ProformaID',$ProformaID)
      ->get();
      $UserTypeID = $result[0]->UserTypeID;
      $Permission = DB::table('UserTypePermission')
      ->where('UserTypeID',$UserTypeID)
      ->where('PermissionID',"CreateQuotationShowName")
      ->get();

      if(count($Permission) == 0)
      {     $ShowName = false;
      }
      else{
          $ShowName = true;
      }
      $UserTypeID2 = $result2[0]->UserTypeID;
      $Permission2 = DB::table('UserTypePermission')
      ->where('UserTypeID',$UserTypeID)
      ->where('PermissionID',"CreateQuotationShowName")
      ->get();

      if(count($Permission2) == 0)
      {     $ShowName2 = false;
      }
      else{
          $ShowName2 = true;
      }
      $QuotationID = $result[0]->QuotationID;
      $BranchID = $result[0]->BranchID;
      $TotalPayment = DB::table('InvoicePayment')
      ->where('ProformaID',$ProformaID)
      ->sum('Paid');
      $QuotationID2 = $result2[0]->QuotationID;
      $BranchID2 = $result2[0]->BranchID;
      // return $QuotationID;
        if($BranchID === null)
        {

            $item =$result[0];
            // return $item;
            $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
            $hardware = DB::table('QuotationDetailHardware')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
            ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                             sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price"'))
            ->where('Quotation.QuotationID', $QuotationID)
            ->groupby('QuotationDetailHardware.HardwareID')
            ->groupby('HardwareName')
            ->groupby('Hardware.Price')
            ->get();

            $item->hardware = $hardware;

            $license = DB::table('QuotationDetailLicense')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
            ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
            ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                             "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price"'))
            ->where('Quotation.QuotationID', $QuotationID)
            ->get();
            $item->license = $license;

            $service = DB::table('QuotationDetailService')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
            ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(['QuotationDetailService.ServiceID','ServiceName','QuotationDetailService.SubTotal','QuotationDetailService.Quantity','DiscountTotal',
            'QuotationDetailService.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','QuotationDetailService.ResellerPrice','SubTotalReseller',
            'PriceTotal'])
            ->where('Quotation.QuotationID', $QuotationID)
            ->get();
            $item->service = $service;

            $item->ShowName = $ShowName;

            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
                'InvoiceDetail' => $result
            );

             return Response()->json($endresult);
        }
        else{
            $item =$result2[0];
            $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
            $hardware = DB::table('QuotationDetailHardware')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
            ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                             sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price"'))
            ->where('Quotation.QuotationID', $QuotationID2)
            ->where('BranchID',$BranchID2)
            ->groupby('QuotationDetailHardware.HardwareID')
            ->groupby('HardwareName')
            ->groupby('Hardware.Price')
            ->get();

            $item->hardware = $hardware;

            $license = DB::table('QuotationDetailLicense')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
            ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
            ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                             "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price"'))
            ->where('Quotation.QuotationID', $QuotationID2)
            ->where('QuotationDetail.BranchID',$BranchID2)
            ->get();
            $item->license = $license;

            $service = DB::table('QuotationDetailService')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
            ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(['QuotationDetailService.ServiceID','ServiceName','QuotationDetailService.SubTotal','QuotationDetailService.Quantity','DiscountTotal',
            'QuotationDetailService.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','QuotationDetailService.ResellerPrice','SubTotalReseller',
            'PriceTotal'])
            ->where('Quotation.QuotationID', $QuotationID)
            ->where('QuotationDetail.BranchID',$BranchID2)
            ->get();
            $item->service = $service;
            $item->ShowName = $ShowName2;
            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
                'InvoiceDetail' => $result2
            );

             return Response()->json($endresult);
        }
    }
      // return $item;

      public function getInvoiceDetailExcel(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'isGetAll' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $isGetAll = $input['isGetAll'];

        $UserID = $this->param->UserID;
        $isGetAll = $input['isGetAll'];

        if($isGetAll === true){
            $header = DB::table('Invoice')
            ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
            ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('QuotationDetailUser','Quotation.QuotationID','=','QuotationDetailUser.QuotationID')
            ->leftjoin('User', 'QuotationDetailUser.UserID','=','User.UserID')
            ->leftjoin('UserType','UserType.UserTypeID','=','User.UserTypeID')
            ->leftjoin('Status', 'Invoice.StatusID','=','Status.StatusID')
            ->leftjoin('Branch','Branch.BranchID','=','Invoice.BranchID')
            ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO','UserFullName',
            'QuotationDetailUser.UserID','Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName','Branch.Contact AS BranchContact','Branch.Phone AS BranchPhone',
            'StatusName','DeliveryDate','DueDate','PaidDate', 'User.UserTypeID','UserTypeName','Brand.Contact AS BrandContact','Brand.Phone AS BrandPhone', 'Brand.Email','Brand.Address',
            'Invoice.GrandTotalDiscount','Invoice.URL','ProformaID','ProformaNO','ReceiptID','ReceiptNO','PONumber','Note'])
            ->orderby('ProformaID','desc')
            ->get();

            $headercount = count($header);

            for($i=0;$i<$headercount;$i++)
            {
                $ProformaID = $header[$i]->ProformaID;

                $UserTypeID = $header[$i]->UserTypeID;
                $Permission = DB::table('UserTypePermission')
                ->where('UserTypeID',$UserTypeID)
                ->where('PermissionID',"CreateQuotationShowName")
                ->get();

                if(count($Permission) == 0)
                {
                    $ShowName = false;
                }
                else{
                    $ShowName = true;
                }

                $QuotationID = $header[$i]->QuotationID;
                $BranchID = $header[$i]->BranchID;
                $TotalPayment = DB::table('InvoicePayment')
                ->where('ProformaID',$ProformaID)
                ->sum('Paid');

                if($BranchID === null)
                {

                    $item =$header[$i];
                    // return $item;
                    $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
                    $hardware = DB::table('QuotationDetailHardware')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                    ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                                     sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->groupby('QuotationDetailHardware.HardwareID')
                    ->groupby('HardwareName')
                    ->groupby('Hardware.Price')
                    ->get();

                    $item->hardware = $hardware;

                    $license = DB::table('QuotationDetailLicense')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                    ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                    ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                                     "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->get();
                    $item->license = $license;
                    $service = DB::table('QuotationDetailService')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                    ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailService"."ServiceID","ServiceName",
                                     sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Service"."Price"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->groupby('QuotationDetailService.ServiceID')
                    ->groupby('ServiceName')
                    ->groupby('Service.Price')
                    ->get();
                    $item->service = $service;

                    $item->ShowName = $ShowName;

                }
                else{
                    $item = $header[$i];
                    $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
                    $hardware = DB::table('QuotationDetailHardware')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                    ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                                     sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('BranchID',$BranchID)
                    ->groupby('QuotationDetailHardware.HardwareID')
                    ->groupby('HardwareName')
                    ->groupby('Hardware.Price')
                    ->get();

                    $item->hardware = $hardware;

                    $license = DB::table('QuotationDetailLicense')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                    ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                    ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                                     "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->get();
                    $item->license = $license;
                    $service = DB::table('QuotationDetailService')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                    ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailService"."ServiceID","ServiceName",
                                     sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Service"."Price"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('BranchID',$BranchID)
                    ->groupby('QuotationDetailService.ServiceID')
                    ->groupby('ServiceName')
                    ->groupby('Service.Price')
                    ->get();
                    $item->service = $service;
                    $item->ShowName = $ShowName;

                }

            }
            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
                'Header' => $header,
                'isgetall' => "True");
        }
        else{
            $header = DB::table('Invoice')
            ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
            ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('QuotationDetailUser','Quotation.QuotationID','=','QuotationDetailUser.QuotationID')
            ->leftjoin('User', 'QuotationDetailUser.UserID','=','User.UserID')
            ->leftjoin('UserType','UserType.UserTypeID','=','User.UserTypeID')
            ->leftjoin('Status', 'Invoice.StatusID','=','Status.StatusID')
            ->leftjoin('Branch','Branch.BranchID','=','Invoice.BranchID')
            ->where('QuotationDetailUser.UserID',$UserID)
            ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO','UserFullName',
            'QuotationDetailUser.UserID','Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName','Branch.Contact AS BranchContact','Branch.Phone AS BranchPhone',
            'StatusName','DeliveryDate','DueDate','PaidDate', 'User.UserTypeID','UserTypeName','Brand.Contact AS BrandContact','Brand.Phone AS BrandPhone', 'Brand.Email','Brand.Address',
            'Invoice.GrandTotalDiscount','Invoice.URL','ProformaID','ProformaNO','ReceiptID','ReceiptNO','PONumber','Note'])
            ->orderby('ProformaID','desc')
            ->get();
            $headercount = count($header);

                        for($i=0;$i<$headercount;$i++)
                        {
                            $ProformaID = $header[$i]->ProformaID;

                            $UserTypeID = $header[$i]->UserTypeID;
                            $Permission = DB::table('UserTypePermission')
                            ->where('UserTypeID',$UserTypeID)
                            ->where('PermissionID',"CreateQuotationShowName")
                            ->get();

                            if(count($Permission) == 0)
                            {
                                $ShowName = false;
                            }
                            else{
                                $ShowName = true;
                            }

                            $QuotationID = $header[$i]->QuotationID;
                            $BranchID = $header[$i]->BranchID;
                            $TotalPayment = DB::table('InvoicePayment')
                            ->where('ProformaID',$ProformaID)
                            ->sum('Paid');

                            if($BranchID === null)
                            {

                                $item =$header[$i];
                                // return $item;
                                $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
                                $hardware = DB::table('QuotationDetailHardware')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                                ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                                                 sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->groupby('QuotationDetailHardware.HardwareID')
                                ->groupby('HardwareName')
                                ->groupby('Hardware.Price')
                                ->get();

                                $item->hardware = $hardware;

                                $license = DB::table('QuotationDetailLicense')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                                ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                                ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                                                 "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->get();
                                $item->license = $license;
                                $service = DB::table('QuotationDetailService')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                                ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailService"."ServiceID","ServiceName",
                                                 sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Service"."Price"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->groupby('QuotationDetailService.ServiceID')
                                ->groupby('ServiceName')
                                ->groupby('Service.Price')
                                ->get();
                                $item->service = $service;
                                $item->ShowName = $ShowName;



                            }
                            else{
                                $item =$header[$i];
                                $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
                                $hardware = DB::table('QuotationDetailHardware')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                                ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                                                 sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->where('BranchID',$BranchID)
                                ->groupby('QuotationDetailHardware.HardwareID')
                                ->groupby('HardwareName')
                                ->groupby('Hardware.Price')
                                ->get();

                                $item->hardware = $hardware;

                                $license = DB::table('QuotationDetailLicense')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                                ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                                ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                                                 "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->where('QuotationDetail.BranchID',$BranchID)
                                ->get();
                                $item->license = $license;
                                $service = DB::table('QuotationDetailService')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                                ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailService"."ServiceID","ServiceName",
                                                 sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Service"."Price"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->where('BranchID', $BranchID)
                                ->groupby('QuotationDetailService.ServiceID')
                                ->groupby('ServiceName')
                                ->groupby('Service.Price')
                                ->get();
                                $item->service = $service;
                                $item->ShowName = $ShowName;

                            }

                        }
                        $endresult = array(
                            'Status' => 0,
                            'Errors' => array(),
                            'Message' => "Success",
                            'Header' => $header);
        }

        // return $headercount;


        return Response()->json($endresult);

        // return $QuotationID;


    }

    public function getInvoicePreview(request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'QuotationID' => 'required',
          'SplitBill' => 'required'

      ];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $QuotationID = $input['QuotationID'];
      $SplitBill = $input['SplitBill'];
     if($SplitBill == true)
     {
         $header = DB::table('Quotation')
         ->leftjoin('Brand', 'Quotation.BrandID','=','Brand.BrandID')
         ->leftjoin('QuotationDetailUser', 'QuotationDetailUser.QuotationID','=' ,'Quotation.QuotationID')
         ->leftjoin('User','QuotationDetailUser.UserID','=','User.UserID')
         ->leftjoin('QuotationDetail','Quotation.QuotationID','=','QuotationDetail.QuotationID')
         ->leftjoin('Branch','Branch.BranchID','=','QuotationDetail.BranchID')
         ->select(['QuotationDetail.BranchID','BranchName','Branch.Phone','Branch.Address','Branch.Contact','Total','DiscountTotalBranch','Branch.Email',
                    'QuotationDetailUser.UserID','UserFullName','Quotation.QuotationID','QuotationNO'])
         ->where('Quotation.QuotationID',$QuotationID)
         ->get();
         // return $headerbranch;
         for($i = 0; $i<count($header);$i++)
         {
             $item = $header[$i];
             $hardware = DB::table('QuotationDetailHardware')
             ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
             ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
             ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
             ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                              sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price"'))
             ->where('Quotation.QuotationID', $QuotationID)
             ->where('BranchID',$header[$i]->BranchID)
             ->groupby('QuotationDetailHardware.HardwareID')
             ->groupby('HardwareName')
             ->groupby('Hardware.Price')
             ->get();
             $item->hardware = $hardware;
             $license = DB::table('QuotationDetailLicense')
             ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
             ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
             ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
             ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
             ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free",
                              "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price"'))
             ->where('Quotation.QuotationID', $QuotationID)
             ->where('QuotationDetail.BranchID',$header[$i]->BranchID)
             ->get();
             $item->license = $license;
             $service = DB::table('QuotationDetailService')
             ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
             ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
             ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
             ->select(['QuotationDetailService.ServiceID','ServiceName','QuotationDetailService.SubTotal','QuotationDetailService.Quantity','DiscountTotal',
             'QuotationDetailService.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','QuotationDetailService.ResellerPrice','SubTotalReseller',
             'PriceTotal'])
             ->where('Quotation.QuotationID', $QuotationID)
             ->where('QuotationDetail.BranchID',$header[$i]->BranchID)
             ->get();
             $item->service = $service;
         }
     }
     else{
         $header = DB::table('Quotation')
         ->leftjoin('Brand', 'Quotation.BrandID','=','Brand.BrandID')
         ->leftjoin('QuotationDetailUser', 'QuotationDetailUser.QuotationID','=' ,'Quotation.QuotationID')
         ->leftjoin('User','QuotationDetailUser.UserID','=','User.UserID')
         ->leftjoin('Status','Quotation.StatusID','=','Status.StatusID')
         ->select(['Quotation.QuotationID','QuotationNO', 'Quotation.BrandID','BrandName','Brand.Contact','Brand.Email','GrandTotal','GrandTotalReseller','Brand.Address', 'UserFullName','Quotation.StatusID',
                   'StatusName','UserFullName','QuotationDetailUser.UserID','Quotation.QuotationDueDate','Quotation.QuotationDate','Brand.Phone',
                   'GrandTotalDiscount'])
         ->where('Quotation.QuotationID',$QuotationID)
         ->get();
         $item = $header[0];
         $hardware = DB::table('QuotationDetailHardware')
         ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
         ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
         ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
         ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                          sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price"'))
         ->where('Quotation.QuotationID', $QuotationID)
         ->groupby('QuotationDetailHardware.HardwareID')
         ->groupby('HardwareName')
         ->groupby('Hardware.Price')
         ->get();
         $item->hardware = $hardware;
         $license = DB::table('QuotationDetailLicense')
         ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
         ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
         ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
         ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
         ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free",
                          "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price"'))
         ->where('Quotation.QuotationID', $QuotationID)
         ->get();
         $item->license = $license;
         $service = DB::table('QuotationDetailService')
         ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
         ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
         ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
         ->select(['QuotationDetailService.ServiceID','ServiceName','QuotationDetailService.SubTotal','QuotationDetailService.Quantity','DiscountTotal',
         'QuotationDetailService.Price','DiscountID','DiscountName','DiscountPercentage','DiscountValue','QuotationDetailService.ResellerPrice','SubTotalReseller',
         'PriceTotal'])
         ->where('Quotation.QuotationID', $QuotationID)
         ->get();
         $item->service = $service;

     }


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'InvoicePreview' => $header
          );

       return Response()->json($endresult);

    }

    public function insertUpdateInvoice(request $request){
      $input = json_decode($request->getContent(), true);
      $rules = [
          'QuotationID' => 'required',
          'SplitBill' => 'required',
          'PONumber' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      // $result = $this->getInvoice2(false);
      // return $result;
      $ID = @$input['ProformaID'];
      $StatusID = "P";
      $year = date('Y');
      $month = date('m');
      $num = DB::table('Invoice')
      ->whereraw('extract(month from "DeliveryDate") ='.$month.' and extract(year from "DeliveryDate") ='.$year)
      ->max('ProformaNO');
      $num = substr($num,15);
      $num = $num+1;

      $DeliveryDate = now();
      $DueDate = Carbon::now()->addDays(10);
      $QuotationID = $input['QuotationID'];
      $SplitBill = $input['SplitBill'];
      if($ID == null){
          if($SplitBill === true){
              //hitung ada berapa branchnya.
              $Branch = DB::table('QuotationDetail')
              ->where('QuotationID',$QuotationID)
              ->select(['BranchID','Total','DiscountTotalBranch'])
              ->get();
              $BranchCount =  count($Branch);


              //insert tiap branch untuk jadi invoice nya.
              for($i = 0; $i<$BranchCount;$i++)
              {    $num = $num+$i;
                  $count = str_pad($num, 6, '0', STR_PAD_LEFT);
                  $code = 'HB-PINV-'.$year.'-'.$month.($count);
                  $InvoiceNO = $code;
                  // return $InvoiceNO;
                  $param = array(
                      'ProformaNO' => $InvoiceNO,
                      'QuotationID' => $QuotationID,
                      'DeliveryDate' => $DeliveryDate,
                      'DueDate' => $DueDate,
                      'StatusID' => "P",
                      'BranchID' => $Branch[$i]->BranchID,
                      'GrandTotal' => $Branch[$i]->Total,
                      'GrandTotalDiscount' => $Branch[$i]->DiscountTotalBranch,
                      'PONumber' => $input['PONumber'],
                      'Note' => @$input['Note']
                  );
                  // return $Branch;
                  $result = DB::table('Invoice')
                  ->insert($param);
                  $ID[$i] = $this->getLastVal();
                  $StatusQuotation = "I";
                  $StatusInvoice = "P";
                  $result = DB::table('Invoice')
                  ->where('ProformaID', $ID[$i])
                  ->update(array('DeliveryDate' => $DeliveryDate,'DueDate' => $DueDate, 'StatusID' => $StatusInvoice));

                  $StatusQuotation = DB::table('Quotation')
                  ->where('QuotationID',$QuotationID)
                  ->update(array('StatusID' => $StatusQuotation));

              }

          }
          else{
              $Total = DB::table('Quotation')
              ->where('QuotationID',$QuotationID)
              ->select(['GrandTotal','GrandTotalDiscount'])
              ->get();


              $num = $num;
              $count = str_pad($num, 6, '0', STR_PAD_LEFT);
              $code = 'HB-PINV-'.$year.'-'.$month.($count);
              $InvoiceNO = $code;

              $param = array(
                  'ProformaNO' => $InvoiceNO,
                  'QuotationID' => $QuotationID,
                  'DeliveryDate' => $DeliveryDate,
                  'DueDate' => $DueDate,
                  'StatusID' => "P",
                  'GrandTotal' => $Total[0]->GrandTotal,
                  'GrandTotalDiscount' => $Total[0]->GrandTotalDiscount,
                  'PONumber' => $input['PONumber'],
                  'Note' => @$input['Note']
              );
              $result = DB::table('Invoice')
              ->insert($param);
              $ID[0] = $this->getLastVal();
              $StatusQuotation = "I";
              $StatusInvoice = "P";
              $result = DB::table('Invoice')
              ->where('ProformaID', $ID[0])
              ->update(array('DeliveryDate' => $DeliveryDate,'DueDate' => $DueDate, 'StatusID' => $StatusInvoice));

              $StatusQuotation = DB::table('Quotation')
              ->where('QuotationID',$QuotationID)
              ->update(array('StatusID' => $StatusQuotation));

          }

      }
      else{
          $code = 'HB-PINV-'.$year.'-'.$month.($count);
          $InvoiceNO = $code;

          $param = array(
              'ProformaNO' => $InvoiceNO,
              'QuotationID' => $QuotationID,
              'DeliveryDate' => $DeliveryDate,
              'DueDate' => $DueDate,
              'StatusID' => "P",
              'GrandTotal' => @$input['GrandTotal'],
              'BranchID' => @$input['BranchID'],
              'GrandTotalDiscount' => @$input['GrandTotalDiscount'],
              'Note' => @$input['Note']
          );
          $result = DB::table('Invoice')
          ->insert($param);
          $ID[0] = $this->getLastVal();
          $StatusQuotation = "I";
          $StatusInvoice = "P";
          $result = DB::table('Invoice')
          ->where('ProformaID', $ID)
          ->update(array('DeliveryDate' => $DeliveryDate,'DueDate' => $DueDate, 'StatusID' => $StatusInvoice));

          $StatusQuotation = DB::table('Quotation')
          ->where('QuotationID',$QuotationID)
          ->update(array('StatusID' => $StatusQuotation));


      }



          $result = $this->checkReturn($result);
          $result['ProformaID'] = $ID;
          return Response()->json($result);

    }

    public function updateInvoiceStatus(request $request){
      $input = json_decode($request->getContent(), true);
      $rules = [
          'ProformaID' => 'required',
          'Status' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }

      $ProformaID = $input['ProformaID'];
      $Status = $input['Status'];
      $DeliveryDate = now();
      $DueDate = Carbon::now()->addDays(30);

      if($Status == "S")
      {$result = DB::table('Invoice')
      ->where('ProformaID', $ProformaID)
      ->update(array('DeliveryDate' => $DeliveryDate,'DueDate' => $DueDate, 'Status' => $Status));
      }
      Elseif($Status == "ID")
      {$result = DB::table('Invoice')
      ->where('ProformaID', $ProformaID)
      ->update(array('Status' => $Status));}

          $result = $this->checkReturn($result);
          return Response()->json($result);
    }

     public function DeleteQuotation(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $QuotationID = @$input['QuotationID'];
       $result = DB::table('Quotation')->where('QuotationID', $QuotationID)->update(array(
                  'Status' => 'D',
           ));

      $result = $this->checkReturn($result);

      return Response()->json($result);
    }
}
