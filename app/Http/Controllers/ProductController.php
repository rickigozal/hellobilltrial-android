<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class ProductController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getProduct(){
      $result = DB::table('Product')
      ->select(['ProductID','ProductCode','ProductName'])
      ->where('Status',null)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Product' => $result
      );
    return Response()->json($endresult);
    }

    public function getProductDetail(Request $request){

      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'ProductID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $ProductID = $input['ProductID'];
      $result = DB::table('Product')->select(['ProductID','ProductCode','ProductName'])
      ->where('ProductID',$ProductID)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Product' => $result
      );
      return Response()->json($endresult);
}

public function InsertUpdateProduct(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'ProductCode' => 'required',
        'ProductName' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $ID = @$input["ProductID"];
    $unique = array(
        'Table' => "Product",
        'ID' => $ID,
        'Column' => "ProductName",
        'String' => $input['ProductName']
    );
    $uniqueProductName = $this->unique($unique);
    $unique['Column'] = "ProductCode";
    $unique['String'] = $input['ProductCode'];
    $uniqueProductCode = $this->unique($unique);

    $param = array(
        'ProductCode' => $input['ProductCode'],
        'ProductName' => $input['ProductName']
      );

      if ($ID == null){$result = DB::table('Product')->insert($param);}
      else {$result = DB::table('Product')->where('ProductID', $ID)->update($param);}

    $result = $this->checkReturn($result);
    return Response()->json($result);

  }

  public function DeleteProduct(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $rules = [
         'ProductID' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $ProductID = @$input['ProductID'];
       $param = array('Status' => 'D','Archived' => now());
       $result = DB::table('Product')->where('ProductID', $ProductID)->update($param);

      $result = $this->checkReturn($result);

      return Response()->json($result);

  }


}
