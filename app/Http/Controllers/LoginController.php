<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Mail\ForgetPassword;
use Mail;
use DB;
use Validator;

class LoginController extends Controller
{
    public function login(Request $request){

        $input = json_decode($request->getContent(),true);
        $rules = [
                    'EmailUsername' => 'required',
                    'Password' => 'required'
        ];


        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $EmailUsername = $input['EmailUsername'];
        $password = collect(\DB::select('select "Password", "UserID", "UserTypeID"
                                from "User"
                                where "Archived" is null
                                AND ("Email" = :email OR "Username" = :email)',
                                array(
                                    'email' => $input['EmailUsername'],

                                )))->first();
                                
        if($password == null){
            $result = array(
                'Status' => 1,
                'Errors' => array(),
                'Message' => "Login Fail"
            );
        } else {
            if(password_verify(@$input['Password'], $password->Password)){
                $token = '';
                $pool = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));

                for($i=0; $i < 36; $i++) {
                    $token .= $pool[mt_rand(0, count($pool) - 1)];
                }

                $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
                $result = DB::table('Authenticator')->insert(
                    array(
                        'AuthenticatorToken' => $token,
                        'UserID' => $password->UserID,
                        'CreatedDate' => $now
                    )
                );


                // $result = DB::select('select "PermissionID"
                //                 from "UserTypePermission"
                //                 where "UserTypeID" = :UserTypeID',
                //                 array(
                //                     'UserTypeID' => $password->UserTypeID
                //                 ));

                $UserData = DB::table('User')
                ->select(['UserFullName', 'UserID','Email','Address','User.UserTypeID',
                'UserTypeName'])
                ->leftjoin('UserType', 'User.UserTypeID','=','UserType.UserTypeID')
                ->where('Email', $EmailUsername)
                ->orwhere('Username',$EmailUsername)
                ->get();

                $permission= DB::table('UserTypePermission')
                ->leftjoin('UserType','UserType.UserTypeID','=','UserTypePermission.UserTypeID')
                ->leftjoin('User','User.UserTypeID','=','UserType.UserTypeID')
                ->leftjoin('Permission','Permission.PermissionID','=','UserTypePermission.PermissionID')
                ->select(['Permission.PermissionID','PermissionName','PermissionType','Entry','ParentID','Permission.URL'])
                ->whereNotNull('Entry')
                ->where('User.UserID', $password->UserID)
                ->orderby('Permission.Number', 'asc')
                ->get();
                //
                // $parent = DB::table('UserTypePermission')
                // ->leftjoin('UserType','UserType.UserTypeID','=','UserTypePermission.UserTypeID')
                // ->leftjoin('User','User.UserTypeID','=','UserType.UserTypeID')
                // ->leftjoin('Permission','Permission.PermissionID','=','UserTypePermission.PermissionID')
                // ->distinct('ParentID')
                // ->select(['ParentID','Icon','URL'])
                // ->where('Entry','<>',null)
                // ->where('Icon','<>',null)
                // ->where('User.UserID', $password->UserID)
                // ->get();

                $parent = DB::table('Parent')
                ->leftjoin('Permission','Permission.ParentID','=','Parent.ParentID')
                ->leftjoin('UserTypePermission','UserTypePermission.PermissionID','=','Permission.PermissionID')
                ->leftjoin('UserType','UserTypePermission.UserTypeID','=','UserType.UserTypeID')
                ->leftjoin('User','UserType.UserTypeID','=','User.UserTypeID')
                ->distinct('Parent.ParentID')
                ->select(['Parent.ParentID','ParentName','Parent.URL','Parent.Icon'])
                ->where('UserID',$password->UserID)
                ->orderby('ParentName','asc')
                ->get();
                // return $parent;

                foreach($parent as $k=>$v){
                  $v->Permissions = [];
                  $entry[$v->ParentID] = $v;
                  unset($entry[$k]);
                }

                foreach($permission as $v){
                  $entry[$v->ParentID]->Permissions[]=$v;
                }

                $parent = array_values(json_decode(json_encode($parent), true));
                // return $parent;
                $result = array(
                    'Status' => 0,
                    'Errors' => array(),
                    'Token' => $token,
                    'Message' => "Login Success",
                    'UserData' => $UserData,
                    'Permission' => $parent
                );
            } else {
                $result = array(
                    'Status' => 1,
                    'Errors' => array(),
                    'Message' => "Login Fail"
                );
            }

        }

        return Response()->json($result);
  }

  public function forgetPassword(Request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'Email' => 'required|email'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $ID = null;
      $unique = array(
          'Table' => "User",
          'ID' => $ID,
          'Column' => "Email",
          'String' => $input['Email']
      );
      $result = DB::table($unique['Table'])
      ->where(DB::raw('lower("'.$unique['Column'].'")'),strtolower($unique['String']))
      ->where($unique['Table'].'ID','<>',$unique['ID'])
      ->where('Archived', null)
      ->count();

      $password = collect(\DB::select('select "Password", "UserID", "UserTypeID","UserFullName"
                                       from "User"
                                       where "Archived" is null
                                       AND ("Email" = :email)',
                                       array(
                                             'email' => $input['Email'],
                                             )))->first();

      if($result > 0)
      {
          $token = '';
          $pool = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));

          for($i=0; $i < 36; $i++) {
              $token .= $pool[mt_rand(0, count($pool) - 1)];
          }

          $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
          $now30 = collect(\DB::select("Select timezone('Asia/Jakarta', now() + INTERVAL '30 MINUTES') \"ServerTime\""))->first()->ServerTime;
          $result = DB::table('ForgetAuthenticator')->insert(
              array(
                  'ForgetAuthenticatorToken' => $token,
                  'UserID' => $password->UserID,
                  'CreatedDate' => $now,
                  'ExpiredDate' => $now30
              )
          );
          $UserFullName = $password->UserFullName;
          $fullname = $password->UserFullName;
          $data = ['message' => 'This is a test!'];
          $objDemo = new \stdClass();
        $objDemo->link = 'Hellobill.com/forget_password';
        $objDemo->sender = 'HelloBill';
        $objDemo->receiver = $UserFullName;

             Mail::to($input['Email'])->send(new ForgetPassword($objDemo));


          $result = array(
              'Status' => 0,
              'Errors' => array(),
              'Token' => $token,
              'Message' => "Check Your Email"
          );
      } else {
          $result = array(
              'Status' => 1,
              'Errors' => array(),
              'Message' => "Wrong Email"
          );
      }

      return Response()->json($result);
          }

          public function setPassword(request $request){
              $input = json_decode($request->getContent(),true);
              $rules = [
                  'Password' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$/',
                  'Username' => 'required'
              ];

              $validator = Validator::make($input, $rules);
              if ($validator->fails()) {
                  $errors = $validator->errors();
                  $errorList = $this->checkErrors($rules, $errors);
                  $additional = null;
                  $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
                  return response()->json($response);
              }

              $Password = password_hash($input['Password'], PASSWORD_BCRYPT);
              $result = DB::table('User')
              ->where(DB::raw('lower("Username")'),strtolower($input['Username']))
              ->update(array('Password' => $Password));

              $result = $this->checkReturn($result);
              return response()->json($result);

          }





  // public function logout(Request $request){
  //       $input = json_decode($request->getContent(),true);
  //       $AuthenticatorToken = @$input['Token'];
  //       $DisabledDate = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
  //       $result = DB::table('Authenticator')->where('AuthenticatorToken', $AuthenticatorToken)->update(array(
  //                   'DisabledDate' => $DisabledDate
  //            ));
  //
  //       if($result == true){
  //           $result = array(
  //               'Status' => 0,
  //               'Errors' => array(),
  //               'Message' => "Logout Success"
  //           );
  //       } else {
  //           $result = array(
  //               'Status' => 1,
  //               'Errors' => array(),
  //               'Message' => "Logout Fail"
  //           );
  //       }
  //
  //       return Response()->json($result);
  //
  // }


}
