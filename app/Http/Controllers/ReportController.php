<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class ReportController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getCommissionProfit(){
      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'StartDate' => 'required|date',
        'EndDate' => 'required|date'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $UserID = $this->param->UserID;
      $StartDate = $input['StartDate'];
      $EndDate = $input['EndDate'];

      $resultcommission = DB::table('Quotation')
      ->leftjoin('Invoice', 'Invoice.QuotationID','=','Quotation.QuotationID')
      ->leftjoin('SalesVisitation', 'SalesVisitation.SalesVisitationID','=', 'Quotation.SalesVisitationID')
      ->where('Invoice.Status', "Paid")
      ->where('Invoice.PaidDate','>=',$StartDate)
      ->where('Invoice.PaidDate','<=', $EndDate)
      ->where('SalesVisitation.UserID','=',$UserID)

      ->sum('Quotation.Commission');

      $grandtotal = DB::table('Quotation')
      ->leftjoin('Invoice', 'Invoice.QuotationID','=','Quotation.QuotationID')
      ->leftjoin('SalesVisitation', 'SalesVisitation.SalesVisitationID','=', 'Quotation.SalesVisitationID')
      ->where('Invoice.Status', "Paid")
      ->where('SalesVisitation.UserID','=',$UserID)
      ->where('Invoice.PaidDate','>',$StartDate)
      ->where('Invoice.PaidDate','<', $EndDate)
      ->sum('Quotation.GrandTotal');

      $grandtotalreseller = DB::table('Quotation')
      ->leftjoin('Invoice', 'Invoice.QuotationID','=','Quotation.QuotationID')
      ->leftjoin('SalesVisitation', 'SalesVisitation.SalesVisitationID','=', 'Quotation.SalesVisitationID')
      ->where('Invoice.Status', "Paid")
      ->where('SalesVisitation.UserID','=',$UserID)
      ->where('Invoice.PaidDate','>',$StartDate)
      ->where('Invoice.PaidDate','<', $EndDate)
      ->sum('Quotation.GrandTotalReseller');

      if($grandtotalreseller==0)
      {$profit = 0;}
      else{$profit = ($grandtotal-$grandtotalreseller);}


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'TotalCommission' => $resultcommission,
          'TotalProfit' => $profit
      );

       return Response()->json($endresult);
    }

public function getTransactionByUserID(Request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'UserID' => 'required'
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);
  }
  $UserID = $input['UserID'];



  $result = DB::table('Quotation')
  ->leftjoin('Invoice', 'Invoice.QuotationID','=','Quotation.QuotationID')
  ->leftjoin('SalesVisitation', 'SalesVisitation.SalesVisitationID','=', 'Quotation.SalesVisitationID')
  ->leftjoin('User', 'SalesVisitation.UserID' ,'=','User.UserID')
  ->leftjoin('Branch','SalesVisitation.BranchID','=','Branch.BranchID')
  ->leftjoin('UserType','User.UserTypeID','=','UserType.UserTypeID')
  ->select(['InvoiceNO','GrandTotal','Commission','GrandTotalReseller','PaidDate','UserFullName',
            'UserTypeName','BranchName','User.UserID'])
  ->where('SalesVisitation.UserID',$UserID)
  ->where('Invoice.Status',"Paid")
  ->get();

  $endresult = array(
      'Status' => 0,
      'Errors' => array(),
      'Message' => "Success",
      'Brand' => $result
  );
  return Response()->json($endresult);
}

public function getTransaction(Request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'StartDate' => 'required|date',
      'EndDate' => 'required|date'
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);
  }
  $UserID = $this->param->UserID;
  $StartDate = $input['StartDate'];
  $EndDate = $input['EndDate'];


  $result = DB::table('Quotation')
  ->leftjoin('Invoice', 'Invoice.QuotationID','=','Quotation.QuotationID')
  ->leftjoin('SalesVisitation', 'SalesVisitation.SalesVisitationID','=', 'Quotation.SalesVisitationID')
  ->leftjoin('User', 'SalesVisitation.UserID' ,'=','User.UserID')
  ->leftjoin('Branch','SalesVisitation.BranchID','=','Branch.BranchID')
  ->leftjoin('UserType','User.UserTypeID','=','UserType.UserTypeID')
  ->select(['InvoiceNO','GrandTotal','Commission','GrandTotalReseller','PaidDate','UserFullName',
            'UserTypeName','BranchName','User.UserID','PIC','PassedPIC'])
  ->where('Invoice.Status',"Paid")
  ->where('Invoice.PaidDate','>=',$StartDate)
  ->where('Invoice.PaidDate','<=', $EndDate)
  ->get();

  $endresult = array(
      'Status' => 0,
      'Errors' => array(),
      'Message' => "Success",
      'Transaction' => $result
  );
  return Response()->json($endresult);
}

public function getTransactionQuery(Request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'Query' => 'nullable',
      'Column' => 'required',
      'Sorting' => 'required',
      'StartDate' => 'required|date',
      'EndDate' => 'required|date'
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);
  }
 $string = @$input['Query'];
 $column = @$input['Column'];
 $sorting = @$input['Sorting'];
 $StartDate = @$input['StartDate'];
 $EndDate = @$input['EndDate'];

if($string === null){$result = DB::table('Quotation')
->leftjoin('Invoice', 'Invoice.QuotationID','=','Quotation.QuotationID')
->leftjoin('SalesVisitation', 'SalesVisitation.SalesVisitationID','=', 'Quotation.SalesVisitationID')
->leftjoin('User', 'SalesVisitation.UserID' ,'=','User.UserID')
->leftjoin('Branch','SalesVisitation.BranchID','=','Branch.BranchID')
->leftjoin('UserType','User.UserTypeID','=','UserType.UserTypeID')
->select(['InvoiceNO','GrandTotal','Commission','GrandTotalReseller','PaidDate','UserFullName',
          'UserTypeName','BranchName','User.UserID','PIC','PassedPIC'])
->where('Invoice.Status',"Paid")
->where('Invoice.PaidDate','>=',$StartDate)
->where('Invoice.PaidDate','<=', $EndDate)
->orderby($column,$sorting)
->get();
}
else{
    $result = DB::table('Quotation')
    ->leftjoin('Invoice', 'Invoice.QuotationID','=','Quotation.QuotationID')
    ->leftjoin('SalesVisitation', 'SalesVisitation.SalesVisitationID','=', 'Quotation.SalesVisitationID')
    ->leftjoin('User', 'SalesVisitation.UserID' ,'=','User.UserID')
    ->leftjoin('Branch','SalesVisitation.BranchID','=','Branch.BranchID')
    ->leftjoin('UserType','User.UserTypeID','=','UserType.UserTypeID')
    ->select(['InvoiceNO','GrandTotal','Commission','GrandTotalReseller','PaidDate','UserFullName',
              'UserTypeName','BranchName','User.UserID','PIC','PassedPIC'])
    ->where('Invoice.Status',"Paid")
    ->where('Invoice.PaidDate','>=',$StartDate)
    ->where('Invoice.PaidDate','<=', $EndDate)
    ->whereRaw('"UserFullName" like \'%'.$string.'%\' or "UserTypeName" like \'%'.$string.'%\' or "BranchName" like \'%'.$string.'%\'
    or "InvoiceNO" like \'$'.$string.'$\' ' )
    ->orderby($column,$sorting)
    ->get();
}


  $endresult = array(
      'Status' => 0,
      'Errors' => array(),
      'Message' => "Success",
      'Transaction' => $result
  );
  return Response()->json($endresult);
}



public function InsertUpdateBrand(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'BrandCode' => 'required',
        'BrandName' => 'required',
        'ProductID' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }

    $param = array(
        'BrandCode' => $input['BrandCode'],
        'BrandName' => $input['BrandName'],
        'Image' => @$input['Image'],
        'ProductID' => @$input['ProductID']);

      $ID = @$input["BrandID"];
      if ($ID == null){$result = DB::table('Brand')->insert($param);}
      else {$result = DB::table('Brand')->where('BrandID', $ID)->update($param);}

    $result = $this->checkReturn($result);
    return Response()->json($result);

  }

  public function DeleteBrand(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $rules = [
         'BrandID' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $BrandID = @$input['BrandID'];
       $param = array('Status' => 'D','Archived' => now());
       $result = DB::table('Brand')->where('BrandID', $BrandID)->update($param);

      $result = $this->checkReturn($result);

      return Response()->json($result);

  }


}
