<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use Mail;
use App\Mail\SendInvoice;

class BranchController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function sendPDF(){
        $objDemo = new \stdClass();
        $objDemo->link = 'Hellobill.com/forget_password';
        $objDemo->sender = 'HelloBill';
        $objDemo->receiver = 'ricki';
        $objDemo->gambar = 's3-us-west-2.amazonaws.com/hellobill-assets/brand/18/100/1529657569nullpetrik.png';
        $objDemo->attachment = 's3-us-west-2.amazonaws.com/hellobill-assets/brand/18/100/1529657569nullpetrik.png';

           Mail::to('rickigozal97@gmail.com')->send(new SendInvoice($objDemo));

       // return Response()->json($result);
    }


    public function getBranch(){
      $result = DB::table('Branch')
      ->leftjoin('Brand','Branch.BrandID','=','Brand.BrandID')
      ->select(['BranchID','BranchName','Branch.BrandID','BrandName','Branch.Status','Phone','Branch.Email','Branch.Contact'])
      ->where('Branch.Status',null)
      ->orderby('BranchID','desc')
      ->get();

       return Response()->json($result);
    }

public function getBranchDetail(Request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'BranchID' => 'required',
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);
  }
  $BranchID = $input['BranchID'];
  $result = DB::table('Branch')
  ->join('Brand','Brand.BrandID', '=','Branch.BrandID' )
  ->select(['BranchID','BranchName','Branch.BrandID','BrandName','Branch.Address','Branch.Phone','Branch.Email','Branch.Contact',
  'Branch.RegionID','Branch.ChannelID'])
  ->where('BranchID',$BranchID)
  ->get();

  if($result == true){
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'BranchDetail' => $result
      );
  } else {
      $endresult = array(
          'Status' => 1,
          'Errors' => array(),
          'Message' => "Fail",
          'BranchDetail' => $result
      );
    }

return Response()->json($endresult);

}

public function getBranchForVisitation(Request $request){

    $input = json_decode($request->getContent(),true);
    $rules = [
        'BrandID' => 'required',
        'isGetAll' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $UserID = $this->param->UserID;
    $BrandID = @$input['BrandID'];
    $isGetAll = $input['isGetAll'];
    $branch = DB::table('SalesVisitation')
    ->select(['BranchID'])
    ->get();
    if(count($branch) == 0)
    {
        $branchID = $branch;
    }
    else{
        for($i = 0;$i < count($branch); $i++){
            $branchID[$i] = $branch[$i]->BranchID;
        }
    }


    if($isGetAll === true){
        $result = DB::table('Branch')
        ->leftjoin('Brand','Branch.BrandID','=','Brand.BrandID')
        ->leftjoin('SalesVisitation', 'SalesVisitation.BranchID','=','Branch.BranchID')
        ->leftjoin('Region','Region.RegionID','=','Branch.RegionID')
        ->leftjoin('Channel','Branch.ChannelID','=','Channel.ChannelID')
        ->select(['Branch.BranchID','BranchName','BrandName','Branch.Address','Branch.Phone','Branch.Email','Branch.Contact','Branch.RegionID',
                  'RegionName','Branch.ChannelID','ChannelName'])
        ->where('Branch.BrandID',$BrandID)
        ->where('Branch.Status',null)
        ->wherenotin('Branch.BranchID',$branchID)
        ->orderby('BranchID','desc')
        ->get();
    }
    else{
        $result = DB::table('Branch')
        ->leftjoin('Region','Branch.RegionID','=','Region.RegionID')
        ->leftjoin('Brand','Branch.BrandID','=','Brand.BrandID')
        ->leftjoin('SalesVisitation', 'SalesVisitation.BranchID','=','Branch.BranchID')
        ->leftjoin('Channel','Branch.ChannelID','=','Channel.ChannelID')
        ->select(['Branch.BranchID','BranchName','BrandName','Branch.Address','Branch.Phone','Branch.Email','Branch.Contact','Branch.RegionID',
                  'RegionName','Branch.ChannelID','ChannelName'])
        ->whereRAW('"Branch"."BrandID" = '.$BrandID.' and "Branch"."Status" is null')
        ->whereRAW('"Branch"."CreatedBy" = '.$UserID.' ')
        ->wherenotin('Branch.BranchID',$branchID)
        ->get();
    }

    // return $result;
//cek isi data branch untuk brand tertentu ada atau tidak.

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Branch' => $result
    );

return Response()->json($endresult);

}

      public function getBranchByBrandID(Request $request){

          $input = json_decode($request->getContent(),true);
          $rules = [
              'BrandID' => 'required',
              'isGetAll' => 'required'
          ];

          $validator = Validator::make($input, $rules);
          if ($validator->fails()) {
              $errors = $validator->errors();
              $errorList = $this->checkErrors($rules, $errors);
              $additional = null;
              $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
              return response()->json($response);
          }
          $UserID = $this->param->UserID;
          $BrandID = @$input['BrandID'];
          $isGetAll = $input['isGetAll'];
          if($isGetAll === true){
              $result = DB::table('Branch')
              ->leftjoin('Brand','Branch.BrandID','=','Brand.BrandID')
              ->leftjoin('SalesVisitation', 'SalesVisitation.BranchID','=','Branch.BranchID')
              ->leftjoin('Region','Region.RegionID','=','Branch.RegionID')
              ->leftjoin('Channel','Branch.ChannelID','=','Channel.ChannelID')
              ->select(['Branch.BranchID','BranchName','BrandName','Branch.Address','Branch.Phone','Branch.Email','Branch.Contact','Branch.RegionID',
                        'RegionName','Branch.ChannelID','ChannelName'])
              ->where('Branch.BrandID',$BrandID)
              ->where('Branch.Status',null)
              ->orderby('BranchID','desc')
              ->get();
          }
          else{
              $result = DB::table('Branch')
              ->leftjoin('Region','Branch.RegionID','=','Region.RegionID')
              ->leftjoin('Brand','Branch.BrandID','=','Brand.BrandID')
              ->leftjoin('SalesVisitation', 'SalesVisitation.BranchID','=','Branch.BranchID')
              ->leftjoin('Channel','Branch.ChannelID','=','Channel.ChannelID')
              ->select(['Branch.BranchID','BranchName','BrandName','Branch.Address','Branch.Phone','Branch.Email','Branch.Contact','Branch.RegionID',
                        'RegionName','Branch.ChannelID','ChannelName'])
              ->whereRAW('"Branch"."BrandID" = '.$BrandID.' and "Branch"."Status" is null')
              ->whereRAW('"Branch"."CreatedBy" = '.$UserID.' ')
              ->get();
          }

          // return $result;
//cek isi data branch untuk brand tertentu ada atau tidak.

          $endresult = array(
              'Status' => 0,
              'Errors' => array(),
              'Message' => "Success",
              'Branch' => $result
          );

return Response()->json($endresult);

    }


    public function InsertUpdateBranch(Request $request){
       $input = json_decode($request->getContent(), true);
       $rules = [

         'BranchName' => 'required',
         'Address' => 'required',
         'Phone' => 'required|numeric',
         'BrandID' => 'required',
         'Email' => 'required|email',
         'RegionID' =>'required',
         'ChannelID' => 'required',
         'Contact' => 'required'

       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $ID = @$input['BranchID'];
       $UserID = $this->param->UserID;

       $unique = array(
           'Table' => "Branch",
           'ID' => $ID,
           'Column' => "BranchName",
           'String' => $input['BranchName']
       );
       $uniqueBranchName = $this->unique($unique);
       // $unique['Column'] = "Email";
       // $unique['String'] = $input['Email'];
       // $uniqueEmail = $this->unique($unique);
       $RegionID = (int)$input['RegionID'];
       $ChannelID = (int)$input['ChannelID'];
       // return gettype($RegionID);

       if($ChannelID == 0 )
       {
           $count = DB::table('Channel')
           ->max('ChannelID');
           $count = $count+1;
           $unique = array(
               'Table' => "Channel",
               'ID' => $ChannelID,
               'Column' => "ChannelName",
               'String' => $input['ChannelName']
           );
           $result = DB::table($unique['Table'])
           ->where(DB::raw('lower("'.$unique['Column'].'")'),strtolower($unique['String']))
           ->where($unique['Table'].'ID','<>',$unique['ID'])
           ->where('Archived', null)
           ->count();
           if($result>0)
           {
               $result = DB::table($unique['Table'])
               ->where(DB::raw('lower("'.$unique['Column'].'")'),strtolower($unique['String']))
               ->where('Archived', null)
               ->first()->ChannelID;
               $ChannelID = $result;
           }
           else{
               $result = DB::table('Channel')
               ->insert(array('ChannelID' => $count,'ChannelName' => $input['ChannelName']));
               $ChannelID = $count;
           }


       }

       if($RegionID == 0 )
       {
           $count = DB::table('Region')
           ->max('RegionID');
           $count = $count+1;
           $unique = array(
               'Table' => "Region",
               'ID' => $RegionID,
               'Column' => "RegionName",
               'String' => $input['RegionName']
           );
           $result = DB::table($unique['Table'])
           ->where(DB::raw('lower("'.$unique['Column'].'")'),strtolower($unique['String']))
           ->where($unique['Table'].'ID','<>',$unique['ID'])
           ->where('Archived', null)
           ->count();
           if($result>0)
           {
               $result = DB::table($unique['Table'])
               ->where(DB::raw('lower("'.$unique['Column'].'")'),strtolower($unique['String']))
               ->where('Archived', null)
               ->first()->RegionID;
               $RegionID = $result;
           }
           else{
               $result = DB::table('Region')
               ->insert(array('RegionID' => $count,'RegionName' => $input['RegionName']));
               $RegionID = $count;
           }


       }

       $param = array (
         'BranchName' => $input['BranchName'],
         'Address' => $input['Address'],
         'Phone' => $input['Phone'],
         'BrandID' => $input['BrandID'],
         'CreatedBy' => $this->param->UserID,
         'Email' => $input['Email'],
         'Contact' => @$input['Contact'],
         'RegionID' => $RegionID,
         'ChannelID' => $ChannelID,
         'Contact' => @$input['Contact']
       );


       if($ID == null)
       {
       $result = DB::table('Branch')->insert($param);
       $ID = $this->getLastVal();
         }

       else {
         $result = DB::table('Branch')->where('BranchID', $ID)->update($param);
            }

    $result = $this->checkReturn($result);
    $result['BranchID'] = $ID;
    return Response()->json($result);
  }


    public function DeleteBranch(Request $request){
         $input = json_decode($this->request->getContent(),true);
         $rules = [
           'BranchID' => 'required'
         ];
         $validator = Validator::make($input, $rules);
         if ($validator->fails()) {
             $errors = $validator->errors();
             $errorList = $this->checkErrors($rules, $errors);
             $additional = null;
             $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
             return response()->json($response);
         }
         $BranchID = $input['BranchID'];
         $param = array('Status' => 'D','Archived' => now());
         $result = DB::table('Branch')->where('BranchID', $BranchID)->update($param);


        $result = $this->checkReturn($result);

        return Response()->json($result);

    }
}
