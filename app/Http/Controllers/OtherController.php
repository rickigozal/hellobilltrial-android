<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class OtherController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getOther(){

            $result = DB::table('Other')->select(['OtherID','OtherName','Price'])
            ->where('Archived',null)
            ->orderby('OtherID','asc')
            ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Other' => $result
      );
    return Response()->json($endresult);
    }

    public function getOtherDetail(request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'OtherID' => 'required',
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $OtherID = $input['OtherID'];
      $result = DB::table('Other')
      ->select(['OtherID','OtherName','Price'])
      ->where('OtherID',$OtherID)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Other' => $result
      );
    return Response()->json($endresult);

    }

    public function InsertUpdateOther(request $request){

        $input = json_decode($request->getContent(), true);
        $rules = [
        'OtherName' => 'required',
        'Price' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $ID = @$input['OtherID'];
        $unique = array(
            'Table' => "Other",
            'ID' => $ID,
            'Column' => "OtherName",
            'String' => $input['OtherName']
        );
        $uniqueOtherName = $this->unique($unique);
        $param = array(
            'OtherName' => $input['OtherName'],
            'Price' => $input['Price']
        );

        if($ID == null){$result = DB::table('Other')
        ->insert($param);}
        else {$result = DB::table('Other')
          ->where('OtherID', $ID)
          ->update($param);
        }

        $result = $this->checkReturn($result);

        return Response()->json($result);

      }

      public function DeleteOther(Request $request){
           $input = json_decode($this->request->getContent(),true);
           $rules = ['OtherID' => 'required'];
           $validator = Validator::make($input, $rules);
           if ($validator->fails()) {
               $errors = $validator->errors();
               $errorList = $this->checkErrors($rules, $errors);
               $additional = null;
               $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
               return response()->json($response);
           }
           $OtherID = @$input['OtherID'];
           $param = array('Archived' => now());

           $result = DB::table('Other')->where('OtherID', $OtherID)->update($param);

          $result = $this->checkReturn($result);

          return Response()->json($result);

      }
}
