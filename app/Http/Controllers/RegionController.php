<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class RegionController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getRegion(){
      $result = DB::table('Region')
      ->select(['RegionID','RegionName'])
      ->where('Status',null)
      ->orderby('RegionID','asc')
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Region' => $result
      );

       return Response()->json($endresult);
    }

    public function getRegionDetail(Request $request){

      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'RegionID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $RegionID = $input['RegionID'];
      $result = DB::table('Region')
      ->select(['RegionID','RegionName'])
      ->where('RegionID',$RegionID)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Brand' => $result
      );
      return Response()->json($endresult);
}

public function InsertUpdateRegion(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'RegionName' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $ID = @$input['RegionID'];
    $unique = array(
        'Table' => "Region",
        'ID' => $ID,
        'Column' => "RegionName",
        'String' => $input['RegionName']
    );
    $uniqueRegionName = $this->unique($unique);

    $param = array(
        'RegionName' => $input['RegionName']);

      if(@$input['DeleteImage'] === true){
            $param = ["Image" => null];
        }
      if ($ID == null){
        $result = DB::table('Brand')->insert($param);
        $ID = $this->getLastVal();
        }
      else{$result = DB::table('Brand')->where('BrandID',$ID)->update($param);}

            $result = $this->checkReturn($result);
          // dd($ID);
          // $result['BrandID'] = $ID;
          return Response()->json($result);

  }

  public function DeleteBrand(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $rules = [
         'BrandID' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $BrandID = @$input['BrandID'];
       $param = array('Status' => 'D','Archived' => now());
       $result = DB::table('Brand')->where('BrandID', $BrandID)->update($param);
       $result2 = DB::table('Branch')->where('BrandID',$BrandID)->update($param);
      $result = $this->checkReturn($result);

      return Response()->json($result);

  }

}
