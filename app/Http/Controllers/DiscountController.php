<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class DiscountController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getDiscount(){
      $result = DB::table('Discount')->select(['DiscountID','DiscountCode','DiscountName','DiscountPercentage',
      'DiscountValue', 'StartDate', 'EndDate','Type','Description'])
      ->where('Status',null)
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Discount' => $result
      );
      return Response()->json($endresult);
    }

    public function getDiscountDetail(request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'DiscountID' => 'required',
      ];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $DiscountID = $input['DiscountID'];
      $result = DB::table('Discount')
      ->select(['DiscountID','DiscountName','DiscountCode','StartDate','EndDate','Description','Type','DiscountPercentage',
                'DiscountValue'])
      ->where('DiscountID',$DiscountID)
      ->get();
      $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'Discount' => $result
        );
        return Response()->json($endresult);

    }

    public function getDiscountDetailByDiscountID(Request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'DiscountID' => 'required',
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $DiscountID = $input['DiscountID'];
      $result = DB::table('DiscountDetail')
      ->select(['DiscountDetailID','DiscountDetail.DiscountID','DiscountName','DiscountDetail.HardwareID','HardwareName',
                'DiscountDetail.LicenseID', 'LicenseName','ServiceName','DiscountDetail.ServiceID'])
      ->leftjoin ('Discount', 'DiscountDetail.DiscountID', '=', 'Discount.DiscountID')
      ->leftjoin('Hardware','Hardware.HardwareID', '=', 'DiscountDetail.HardwareID')
      ->leftjoin('License','License.LicenseID', '=', 'DiscountDetail.LicenseID')
      ->leftjoin('Service','Service.ServiceID','=','DiscountDetail.ServiceID')
      ->where('DiscountDetail.DiscountID',$DiscountID)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Discount' => $result
      );
      return Response()->json($endresult);
    }

    public function getDiscountDetailByHardwareID(Request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'HardwareID' => 'required',
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $HardwareID = $input['HardwareID'];
      $result = DB::table('DiscountDetail')
      ->select(['DiscountDetailID','DiscountDetail.DiscountID','DiscountName','DiscountDetail.HardwareID','HardwareName',
                'DiscountPercentage','DiscountValue','Discount.Description'])
      ->leftjoin ('Discount', 'DiscountDetail.DiscountID', '=', 'Discount.DiscountID')
      ->leftjoin('Hardware','Hardware.HardwareID', '=', 'DiscountDetail.HardwareID')
      ->where('DiscountDetail.HardwareID',$HardwareID)
      ->where('Discount.Status',null)
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Discount' => $result
      );
      return Response()->json($endresult);

    }

    public function getDiscountDetailByLicenseID(Request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'LicenseID' => 'required',
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $LicenseID = $input['LicenseID'];
      $result = DB::table('DiscountDetail')
      ->select(['DiscountDetailID','DiscountDetail.DiscountID','DiscountName','DiscountDetail.LicenseID','LicenseName',
                'DiscountPercentage','DiscountValue','Discount.Description'])
      ->leftjoin ('Discount', 'DiscountDetail.DiscountID', '=', 'Discount.DiscountID')
      ->leftjoin('License','License.LicenseID', '=', 'DiscountDetail.LicenseID')
      ->where('DiscountDetail.LicenseID',$LicenseID)
      ->where('Discount.Status',null)
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Discount' => $result
      );
      return Response()->json($endresult);

    }

    public function getDiscountDetailByServiceID(Request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'ServiceID' => 'required',
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $ServiceID = $input['ServiceID'];
      $result = DB::table('DiscountDetail')
      ->select(['DiscountDetailID','DiscountDetail.DiscountID','DiscountName','DiscountDetail.ServiceID','ServiceName',
                'DiscountPercentage','DiscountValue','Discount.Description'])
      ->leftjoin ('Discount', 'DiscountDetail.DiscountID', '=', 'Discount.DiscountID')
      ->leftjoin('Service','Service.ServiceID', '=', 'DiscountDetail.ServiceID')
      ->where('DiscountDetail.ServiceID',$ServiceID)
      ->where('Discount.Status',null)
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Discount' => $result
      );
      return Response()->json($endresult);

    }

    public function InsertUpdateDiscount(Request $request){

      $input = json_decode($request->getContent(), true);
      $rules = [
        'DiscountCode' => 'required',
        'DiscountName' => 'required',
        'StartDate' => 'date|date_format:Y-m-d|nullable',
        'EndDate' => 'date|date_format:Y-m-d|nullable',
        'Type' => 'required',
        'DiscountValue' => 'numeric|nullable',
        'DiscountPercentage' => 'numeric|nullable',
        'HardwareID' => 'distinct|array|nullable',
        'HardwareID' => 'distinct|array|nullable'
      ];


      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }

      $param = array (
        'DiscountCode' => $input['DiscountCode'],
        'DiscountName' => $input['DiscountName'],
        'DiscountPercentage' => @$input['DiscountPercentage'],
        'DiscountValue' => @$input['DiscountValue'],
        'StartDate' => @$input['StartDate'],
        'EndDate' => @$input['EndDate'],
        'Description' => @$input['Description'],
        'Type' => $input['Type']
      );
      $ID = @$input['DiscountID'];

      if($ID == null)
      {$result = DB::table('Discount')->insert($param);
      $ID = $this->getLastVal();
      }

      else
      {$result = DB::table('Discount')->where('DiscountID', $ID)->update($param);
       $result = DB::table('DiscountDetail')->where('DiscountID',$ID)->delete();
      }
 // die();

      // $result = $this->checkReturn($result);
      if ($input['Type'] == 'Hardware'){
          $temp = 'HardwareID';
          for($i = 0; $i < count($input[$temp]);$i++){
              $items = $input[$temp][$i];

              $insertDetail = array(
                'DiscountID' => $ID,
                $temp => @$items
              );
                $resultDetail = DB::table('DiscountDetail')->insert($insertDetail);
              }
      }
      else if ($input['Type'] == 'License'){
          $temp = 'LicenseID';
          for($i = 0; $i < count($input[$temp]);$i++){
              $items = $input[$temp][$i];

              $insertDetail = array(
                'DiscountID' => $ID,
                $temp => @$items
              );
                $resultDetail = DB::table('DiscountDetail')->insert($insertDetail);
              }
      }
      else if ($input['Type'] == 'Service'){
          $temp = 'ServiceID';
          for($i = 0; $i < count($input[$temp]);$i++){
              $items = $input[$temp][$i];

              $insertDetail = array(
                'DiscountID' => $ID,
                $temp => @$items
              );
                $resultDetail = DB::table('DiscountDetail')->insert($insertDetail);
              }
      }
      else{
          $temp1 = 'HardwareID';
          $temp = 'LicenseID';
          $temp2 = 'ServiceID';
          for($i = 0; $i < count(@$input[$temp]);$i++){
              $items = @$input[$temp][$i];
              if(count($items) > 0){
                  $insertDetail = array(
                    'DiscountID' => $ID,
                    $temp => @$items
                  );
                    $resultDetail = DB::table('DiscountDetail')->insert($insertDetail);
              }

              }
              for($i = 0; $i < count(@$input[$temp1]);$i++){
                  $items = @$input[$temp1][$i];
                  if(count($items) > 0){
                      $insertDetail = array(
                        'DiscountID' => $ID,
                        $temp1 => @$items
                      );
                        $resultDetail = DB::table('DiscountDetail')->insert($insertDetail);
                  }

                  }
                  for($i = 0; $i < count(@$input[$temp2]);$i++){
                      $items = @$input[$temp2][$i];
                      if(count($items) > 0){
                          $insertDetail = array(
                            'DiscountID' => $ID,
                            $temp2 => @$items
                          );
                            $resultDetail = DB::table('DiscountDetail')->insert($insertDetail);
                      }

                      }
      }




          $resultDetail = $this->checkReturn($resultDetail);


   return Response()->json($resultDetail);


 }

 public function DeleteDiscount(Request $request){
      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'DiscountID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $DiscountID = @$input['DiscountID'];
      $param = array('Status' => 'D','Archived' => now());
      $result = DB::table('Discount')->where('DiscountID', $DiscountID)->update($param);


    $result = $this->checkReturn($result);
     return Response()->json($result);

 }
}
