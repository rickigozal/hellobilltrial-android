<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use Mail;
use App\Mail\SendInvoice;

class PdfController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function sendInvoicePDF(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'ProformaID' => 'array|required',
            'Base64' => 'array|required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }

        if(@$input['Base64'] !== null){
            $base64mentah = str_replace("\r\n","",@$input['Base64']);

            for($i = 0; $i< count($input['Base64']); $i++)
            {
                $pattern1 = "filename=\"";
                $pos = strpos($base64mentah[$i],$pattern1);
                // return $pos;
                $pos2 = strpos($base64mentah[$i],"pdf\"", $pos);
                $pos2=$pos2+4;
                $base64fix = substr($base64mentah[$i],$pos2);

                $pos3 = strpos($base64mentah[$i],"\"",$pos);
                $length = $pos2-$pos3;

                $filename = substr($base64mentah[$i],$pos3,$length);
                $filename = str_replace("\"","",$filename);

                $arr = array(
                    'UserID' => $this->param->UserID,
                    'ObjectID' => $input['ProformaID'][$i],
                    'Folder' => 'Invoice',
                    'Filename' => $filename,
                    'Data' =>  $base64fix
                );
                // return(@$input['Images']['Data']);
                // die();

                $path = $this->upload_to_s3($arr);
                $data = ['URL' => $path];

                $result = DB::table('Invoice')->where('ProformaID',$input['ProformaID'][$i])->update($data);
                $link[$i] = $path;
                $response = $this->generateResponse(0, [], "Success", ['Invoice'=>$result]);
            }

            }
            $ProformaID = $input['ProformaID'];


        for($i=0;$i<count($input['ProformaID']);$i++){
            $result = DB::table('Invoice')
            ->select(['ProformaID','BranchID'])
            ->where('ProformaID',$input['ProformaID'][$i])
            ->get();

            if($result[0]->BranchID === null)
            {
                $customer = DB::table('Invoice')
                ->leftjoin('Quotation','Invoice.QuotationID','=','Quotation.QuotationID')
                ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
                ->select(['Brand.Email','Contact','BrandName'])
                ->where('ProformaID',$result[0]->ProformaID)
                ->get();

                $customerbranch = DB::table('Invoice')
                ->leftjoin('Quotation','Invoice.QuotationID','=','Quotation.QuotationID')
                ->leftjoin('QuotationDetail','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
                ->select(['BranchName'])
                ->where('ProformaID',$result[0]->ProformaID)
                ->get();

                $pic = DB::table('Invoice')
                ->leftjoin('Quotation','Invoice.QuotationID','=','Quotation.QuotationID')
                ->leftjoin('QuotationDetailUser','QuotationDetailUser.QuotationID','=','Quotation.QuotationID')
                ->leftjoin('User','QuotationDetailUser.UserID','=','User.UserID')
                ->where('ProformaID',$input['ProformaID'][$i])
                ->first()->Email;

                $ccemail = DB::table('User')
                ->leftjoin('UserType','UserType.UserTypeID','=','User.UserTypeID')
                ->leftjoin('UserTypePermission','UserType.UserTypeID','=','UserTypePermission.UserTypeID')
                ->where('PermissionID',"ReceiveEmail")
                ->where('User.Archived',null)
                ->get();

                for($j=0;$j<count($ccemail);$j++)
                {
                    $objDemo->ccemail[$j] = $ccemail[$j]->Email;
                }
                $email = $objDemo->ccemail;
                
                $objDemo = new \stdClass();
                $objDemo->sender = 'HelloBill';
                $objDemo->receiver = $customer[0]->Contact;
                $objDemo->attachment = $link[$i];
                $objDemo->pic = $pic;
                $objDemo->filename = $filename;
                $objDemo->brandname = $customer[0]->BrandName;
                $objDemo->branchname = $customerbranch[0]->BranchName;
                $objDemo->countcc = count($ccemail);

                Mail::to($customer[0]->Email)->cc($email)
                ->send(new SendInvoice($objDemo));
            }
            else{

                $customer = DB::table('Invoice')
                ->leftjoin('Quotation','Invoice.QuotationID','=','Quotation.QuotationID')
                ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
                ->leftjoin('QuotationDetail','QuotationDetail.QuotationID','=','Quotation.QuotationID')
                ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                ->select(['Branch.Email','Branch.Contact','BranchName','BrandName'])
                ->where('ProformaID',$input['ProformaID'][$i])
                ->where('QuotationDetail.BranchID',$result[0]->BranchID)
                ->get();


                $pic = DB::table('Invoice')
                ->leftjoin('Quotation','Invoice.QuotationID','=','Quotation.QuotationID')
                ->leftjoin('QuotationDetailUser','QuotationDetailUser.QuotationID','=','Quotation.QuotationID')
                ->leftjoin('User','QuotationDetailUser.UserID','=','User.UserID')
                ->where('Invoice.ProformaID',$input['ProformaID'][$i])
                ->first()->Email;

                $objDemo = new \stdClass();
                $objDemo->sender = 'HelloBill';
                $objDemo->receiver = $customer[0]->Contact;

                $objDemo->attachment = $link[$i];
                $objDemo->pic = $pic;
                $objDemo->filename = $filename;
                $objDemo->brandname = $customer[0]->BrandName;
                $objDemo->branchname = $customer[0]->BranchName;


                Mail::to($customer[0]->Email)->send(new SendInvoice($objDemo));
            }
        }
        $result = $this->checkReturn($result);
       return Response()->json($result);
    }

    public function uploadFile(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'Base64' => 'required',
            'Filename' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $base64 = base64_decode($input['Base64']);
        $Filename = $input['Filename'];
        $ObjectID = "00";

        $arr = array(
            'UserID' => $this->param->UserID,
            'ObjectID' => $ObjectID,
            'Folder' => 'Invoice',
            'Filename' => $Filename,
            'Data' =>  $base64
        );

        $path = $this->upload_to_s3($arr);
        return $path;
    }

    public function getBranch(){
      $result = DB::table('Branch')
      ->leftjoin('Brand','Branch.BrandID','=','Brand.BrandID')
      ->select(['BranchID','BranchName','Branch.BrandID','BrandName','Branch.Status','Phone','Branch.Email','Branch.Contact'])
      ->where('Branch.Status',null)
      ->orderby('BranchID','desc')
      ->get();

       return Response()->json($result);
    }

public function getBranchDetail(Request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'BranchID' => 'required',
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);
  }
  $BranchID = $input['BranchID'];
  $result = DB::table('Branch')
  ->join('Brand','Brand.BrandID', '=','Branch.BrandID' )
  ->select(['BranchID','BranchName','Branch.BrandID','BrandName','Address','Branch.Phone','Branch.Email','Branch.Contact'])
  ->where('BranchID',$BranchID)
  ->get();

  if($result == true){
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'BranchDetail' => $result
      );
  } else {
      $endresult = array(
          'Status' => 1,
          'Errors' => array(),
          'Message' => "Fail",
          'BranchDetail' => $result
      );
    }

return Response()->json($endresult);

}

      public function getBranchByBrandID(Request $request){

          $input = json_decode($request->getContent(),true);
          $rules = [
              'BrandID' => 'required',
              'isGetAll' => 'required'
          ];

          $validator = Validator::make($input, $rules);
          if ($validator->fails()) {
              $errors = $validator->errors();
              $errorList = $this->checkErrors($rules, $errors);
              $additional = null;
              $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
              return response()->json($response);
          }
          $UserID = $this->param->UserID;
          $BrandID = @$input['BrandID'];
          $isGetAll = $input['isGetAll'];
          if($isGetAll === true){
              $result = DB::table('Branch')
              ->leftjoin('Brand','Branch.BrandID','=','Brand.BrandID')
              ->leftjoin('SalesVisitation', 'SalesVisitation.BranchID','=','Branch.BranchID')
              ->select(['Branch.BranchID','BranchName','BrandName','Address','Branch.Phone','Branch.Email','Branch.Contact'])
              ->where('Branch.BrandID',$BrandID)
              ->where('Branch.Status',null)
              ->orderby('BranchID','desc')
              ->get();
          }
          else{
              $result = DB::table('Branch')
              ->leftjoin('Brand','Branch.BrandID','=','Brand.BrandID')
              ->leftjoin('SalesVisitation', 'SalesVisitation.BranchID','=','Branch.BranchID')
              ->select(['Branch.BranchID','BranchName','BrandName','Address','Branch.Phone','Branch.Email','Branch.Contact'])
              ->whereRAW('"Branch"."BrandID" = '.$BrandID.' and "Branch"."Status" is null')
              ->whereRAW('"Branch"."CreatedBy" = '.$UserID.' ')
              ->get();
          }

          // return $result;
//cek isi data branch untuk brand tertentu ada atau tidak.

          $endresult = array(
              'Status' => 0,
              'Errors' => array(),
              'Message' => "Success",
              'Branch' => $result
          );

return Response()->json($endresult);

    }


    public function InsertUpdateBranch(Request $request){
       $input = json_decode($request->getContent(), true);
       $rules = [

         'BranchName' => 'required',
         'Address' => 'required',
         'Phone' => 'required|numeric',
         'BrandID' => 'required',
         'Email' => 'required|email'

       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $ID = @$input['BranchID'];
       $UserID = $this->param->UserID;

       $unique = array(
           'Table' => "Branch",
           'ID' => $ID,
           'Column' => "BranchName",
           'String' => $input['BranchName']
       );
       $uniqueBranchName = $this->unique($unique);
       // $unique['Column'] = "Email";
       // $unique['String'] = $input['Email'];
       // $uniqueEmail = $this->unique($unique);
       $param = array (
         'BranchName' => $input['BranchName'],
         'Address' => $input['Address'],
         'Phone' => $input['Phone'],
         'BrandID' => $input['BrandID'],
         'CreatedBy' => $this->param->UserID,
         'Email' => $input['Email'],
         'Contact' => @$input['Contact']
       );

       if($ID == null)
       {
       $result = DB::table('Branch')->insert($param);
       $ID = $this->getLastVal();
         }

       else {
         $result = DB::table('Branch')->where('BranchID', $ID)->update($param);
            }

    $result = $this->checkReturn($result);
    $result['BranchID'] = $ID;
    return Response()->json($result);
  }


    public function DeleteBranch(Request $request){
         $input = json_decode($this->request->getContent(),true);
         $rules = [
           'BranchID' => 'required'
         ];
         $validator = Validator::make($input, $rules);
         if ($validator->fails()) {
             $errors = $validator->errors();
             $errorList = $this->checkErrors($rules, $errors);
             $additional = null;
             $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
             return response()->json($response);
         }
         $BranchID = $input['BranchID'];
         $param = array('Status' => 'D','Archived' => now());
         $result = DB::table('Branch')->where('BranchID', $BranchID)->update($param);


        $result = $this->checkReturn($result);

        return Response()->json($result);

    }
}
