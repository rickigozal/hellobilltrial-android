<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class BrandController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
    }

    public function getBrand(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
          'isGetAll' => 'required|boolean'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        if($input['isGetAll'] === true)
        {
            $result = DB::table('Brand')
            ->leftjoin('Product', 'Brand.ProductID','=','Product.ProductID')
            ->select(['BrandID','BrandName','Email','Brand.ProductID','ProductName','Contact','Image','Phone','Address','CreatedBy'])
            ->where('Brand.Archived',null)
            ->orderby('BrandID','desc')
            ->get();
        }
        else{
            // $result = DB::select(
            //     'select *
            //     from "Brand"
            //     left join "Product" on "Brand"."ProductID" = "Product"."ProductID"
            //     where ("CreatedBy" is null or "CreatedBy" = '.$this->param->UserID.') and "Brand"."Archived" is null'
            // );

            $result = DB::table('Brand')
            ->leftjoin('Product','Brand.ProductID','=','Product.ProductID')
            ->wherenull('Brand.Archived')
            ->where(function ($query) {
              $query->where('CreatedBy', null)
                    ->orWhere('CreatedBy', $this->param->UserID);})
            ->get();
        }


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Brand' => $result
      );

       return Response()->json($endresult);
    }

    
    public function getBrandDetail(Request $request){

      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'BrandID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $BrandID = $input['BrandID'];
      $result = DB::table('Brand')
      ->leftjoin('Product', 'Brand.ProductID','=','Product.ProductID')
      ->select(['BrandID','BrandName','Email','Brand.ProductID','Contact','ProductName','Image','Phone','Address'])
      ->where('BrandID',$BrandID)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Brand' => $result
      );
      return Response()->json($endresult);
}

public function InsertUpdateBrand(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'BrandName' => 'required',
        'ProductID' => 'required',
        'Email' => 'required|email',
        'Phone' => 'required',
        'Address' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $ID = @$input['BrandID'];
    $unique = array(
        'Table' => "Brand",
        'ID' => $ID,
        'Column' => "BrandName",
        'String' => $input['BrandName']
    );
    $uniqueBrandName = $this->unique($unique);
    // $unique['Column'] = "Email";
    // $unique['String'] = $input['Email'];
    // $uniqueEmail = $this->unique($unique);
    $param = array(
        'BrandName' => $input['BrandName'],
        'ProductID' => @$input['ProductID'],
        'Email' => $input['Email'],
        'Phone' => $input['Phone'],
        'Contact' => @$input['Contact'],
        'Address' => $input['Address']);

      if(@$input['DeleteImage'] === true){
            $param = ["Image" => null];
        }
      if ($ID == null){
        $result = DB::table('Brand')->insert($param);
        $ID = $this->getLastVal();
        }
      else{$result = DB::table('Brand')->where('BrandID',$ID)->update($param);}

      if(@$input['Images'] !== null){
              $arr = array(
                  'UserID' => $this->param->UserID,
                  'ObjectID' => $ID,
                  'Folder' => 'Brand',
                  'Filename' => @$input['Images']['Filename'],
                  'Data' =>  @$input['Images']['Data']
              );
              // return(@$input['Images']['Data']);
              // die();
              $path = $this->upload_to_s3($arr);
              $data = ['Image' => $path];
              $result = DB::table('Brand')->where('BrandID',$ID)->update($data);

              $response = $this->generateResponse(0, [], "Success", ['Brand'=>$result]);
          }

          $result = $this->checkReturn($result);
          // dd($ID);
          // $result['BrandID'] = $ID;
          return Response()->json($result);

  }

  public function DeleteBrand(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $rules = [
         'BrandID' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $BrandID = @$input['BrandID'];
       $param = array('Status' => 'D','Archived' => now());
       $result = DB::table('Brand')->where('BrandID', $BrandID)->update($param);
       $result2 = DB::table('Branch')->where('BrandID',$BrandID)->update($param);
      $result = $this->checkReturn($result);

      return Response()->json($result);

  }

}
