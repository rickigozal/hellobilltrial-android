<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::post('upload_file','PdfController@uploadFile');

Route::post('generate_quotation','QuotationController@generateQuotation');

Route::post('send_invoice_pdf','PdfController@sendInvoicePDF');
Route::post('send_pdf','BranchController@sendPDF');

Route::post('login','LoginController@login');
Route::post('get_brand_pdf','BrandController@getBrandPDF2');

Route::post('change_password','UserController@changePassword');
Route::post('set_forget_password','ForgetPasswordController@setPassword');
Route::post('forget_password', 'LoginController@forgetPassword');
Route::post('set_password','LoginController@setPassword');

Route::post('get_region','RegionController@getRegion');
Route::post('get_channel','ChannelController@getChannel');

Route::post('get_service','ServiceController@getService');
Route::post('get_service_detail','ServiceController@getServiceDetail');
Route::post('insert_update_service','ServiceController@InsertUpdateService');
Route::post('activate_deactivate_service','ServiceController@ActivateDeactivateService');
Route::post('delete_service','ServiceController@DeleteService');

Route::post('get_other','OtherController@getOther');
Route::post('get_other_detail','OtherController@getOtherDetail');
Route::post('insert_update_other','OtherController@InsertUpdateOther');
Route::post('delete_other','OtherController@DeleteOther');

Route::post('get_permission','UserTypePermissionController@getPermission');
Route::post('get_permission_by_user_type_id','UserTypePermissionController@getPermissionbyUserTypeID');
Route::post('get_menu_permission','UserTypePermissionController@getMenuPermission');

Route::post('get_commission_profit','ReportController@getCommissionProfit');
Route::post('get_transaction','ReportController@getTransaction');
Route::post('get_transaction_by_user_id','ReportController@getTransactionByUserID');
Route::post('get_transaction_query','ReportController@getTransactionQuery');

Route::post('get_branch','BranchController@getBranch');
Route::post('get_branch_detail', 'BranchController@getBranchDetail');
Route::post('delete_branch', 'BranchController@DeleteBranch');
Route::post('get_branch_by_brand_id','BranchController@getBranchByBrandID');
Route::post('insert_update_branch','BranchController@InsertUpdateBranch');

Route::post('get_license','LicenseController@getLicense');
Route::post('get_license_detail','LicenseController@getLicenseDetail');
Route::post('insert_update_license','LicenseController@InsertUpdateLicense');
Route::post('delete_license','LicenseController@DeleteLicense');

Route::post('get_user_type','UserTypeController@getUserType');
Route::post('get_user_type_detail','UserTypeController@getUserTypeDetail');
Route::post('insert_update_user_type','UserTypeController@InsertUpdateUserType');
Route::post('delete_user_type','UserTypeController@DeleteUserType');

Route::post('get_user','UserController@getUser');
Route::post('get_user_passed_pic_true','UserController@getUserPassedPicTrue');
Route::post('get_user_detail','UserController@getUserDetail');
Route::post('insert_update_user','UserController@InsertUpdateUser');
Route::post('delete_user','UserController@DeleteUser');

Route::post('get_brand','BrandController@getBrand');
Route::post('insert_update_brand','BrandController@InsertUpdateBrand');
Route::post('get_brand_detail','BrandController@getBrandDetail');
Route::post('delete_brand','BrandController@DeleteBrand');

Route::post('get_product','ProductController@getProduct');
Route::post('get_product_detail','ProductController@getProductDetail');
Route::post('insert_update_product','ProductController@InsertUpdateProduct');
Route::post('delete_product','ProductController@DeleteProduct');

Route::post('get_discount','DiscountController@getDiscount');
Route::post('insert_update_discount','DiscountController@InsertUpdateDiscount');
Route::post('delete_discount','DiscountController@DeleteDiscount');
Route::post('get_discount_detail_by_discount_id','DiscountController@getDiscountDetailByDiscountID');
Route::post('insert_discount_detail','DiscountController@InsertDiscountDetail');
Route::post('get_discount_detail','DiscountController@getDiscountDetail');
Route::post('get_discount_detail_by_hardware_id','DiscountController@getDiscountDetailByHardwareID');
Route::post('get_discount_detail_by_license_id','DiscountController@getDiscountDetailByLicenseID');
Route::post('get_discount_detail_by_service_id','DiscountController@getDiscountDetailByServiceID');

Route::post('get_hardware','HardwareController@getHardware');
Route::post('get_hardware_detail','HardwareController@getHardwareDetail');
Route::post('insert_update_hardware','HardwareController@InsertUpdateHardware');
Route::post('delete_hardware','HardwareController@DeleteHardware');
Route::post('activate_deactivate_hardware','HardwareController@ActivateDeactivateHardware');

Route::post('get_sales_visitation','SalesVisitationController@getSalesVisitation');
Route::post('get_sales_visitation_status','SalesVisitationController@getVisitationStatus');
Route::post('insert_update_sales_visitation','SalesVisitationController@insertUpdateSalesVisitation');
Route::post('insert_update_sales_visitation_detail','SalesVisitationController@insertUpdateSalesVisitationDetail');
Route::post('delete_sales_visitation','SalesVisitationController@DeleteSalesVisitation');
Route::post('get_sales_visitation_detail','SalesVisitationController@getSalesVisitationDetail');
Route::post('get_sales_visitation_detail_by_id','SalesVisitationController@getSalesVisitationDetailByID');
Route::post('get_sales_visitation_detail_by_sales_id','SalesVisitationController@getSalesVisitationDetailBySalesVisitationID');
Route::post('get_branch_for_visitation','BranchController@getBranchForVisitation');


Route::post('get_quotation','QuotationController@getQuotation');
Route::post('get_quotation_detail','QuotationController@getQuotationDetail');
Route::post('get_quotation_detail_by_quotation_id','QuotationController@getQuotationDetailByQuotationID');
Route::post('get_quotation_detail_excel','QuotationController@getQuotationDetailExcel');
Route::post('insert_update_quotation','QuotationController@insertUpdateQuotation');
Route::post('delete_quotation','QuotationController@DeleteQuotation');
Route::post('insert_update_quotation_detail','QuotationController@insertUpdateQuotationDetail');
Route::post('update_quotation_status','QuotationController@updateQuotationStatus');
Route::post('update_quotation_date','QuotationController@updateQuotationDate');

Route::post('get_invoice','InvoiceController@getInvoice');
Route::post('get_invoice_detail','InvoiceController@getInvoiceDetail');
Route::post('get_invoice_detail_excel','InvoiceController@getInvoiceDetailExcel');
Route::post('insert_update_invoice','InvoiceController@insertUpdateInvoice');
Route::post('update_invoice_status','InvoiceController@updateInvoiceStatus');
Route::post('get_invoice_preview','InvoiceController@getInvoicePreview');
Route::post('generate_invoice_id','InvoiceController@generateInvoiceID');

Route::post('insert_update_invoice_payment','InvoicePaymentController@insertUpdateInvoicePayment');
Route::post('get_invoice_payment_by_invoice_id','InvoicePaymentController@getInvoicePaymentDetailByInvoiceID');
Route::post('get_invoice_payment_detail','InvoicePaymentController@getInvoicePaymentDetail');

Route::post('get_invoice_reseller','InvoiceResellerController@getInvoiceReseller');
Route::post('get_invoice_reseller_detail','InvoiceResellerController@getInvoiceResellerDetail');
Route::post('update_invoice_reseller_status','InvoiceResellerController@updateInvoiceResellerStatus');
Route::post('insert_update_invoice_reseller','InvoiceResellerController@insertUpdateInvoiceReseller');
